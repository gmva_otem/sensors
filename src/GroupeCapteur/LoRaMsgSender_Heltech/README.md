# Projet OTEM - Partie capteur

## Context
    L'objectif de notre projet est de récupérer la température que capte le capteur à l’aide du microcontrôleur, de lié cette donnée à une date de prélèvement, que l’on récupère à l’aide d’une horloge RTC. Nous devons également faire en sorte que ce microcontrôleur, soit allumer seulement quand il a des opérations en cours (Prélèvement et transmission des données toutes les 2 heures). Cela permet d’économiser la batterie de celui-ci pour optimiser sa durée de vie. A chaque prélèvement, on transmet les données aà une passerelle.

## Voici les différents fichiers de notre partie du projet.
    • Logger.cpp et Logger.h : Ces fichiers définissent la classe Logger, qui est responsable de la gestion des logs.
    • LoRaManager.cpp et LoRaManager.h : Ces fichiers définissent la classe LoRaManager, qui gère la communication LoRa.
    • LoRaNode.cpp et LoRaNode.h : Ces fichiers définissent la classe LoRaNode, qui représente qui permet la diffision lora.
    • main.cpp : Le fichier principal du projet, contenant la fonction main(). C'est le point d'entrée de l’application.
    • RTCManager.cpp et RTCManager.h : Ces fichiers définissent la classe RTCManager, qui gère l'horloge temps réel (RTC).
    • TemperatureSensor.cpp et TemperatureSensor.h : Ces fichiers définissent la classe TemperatureSensor, qui gère la lecture et l'interprétation des données du capteur de température.
    • TPL5110Manager.cpp et TPL5110Manager.h : Ces fichiers définissent la classe TPL5110Manager, qui gère le Timer TPL5110 pour la gestion de l'alimentation afin d’allumer toutes les 2 heures le micro-contrôleur .