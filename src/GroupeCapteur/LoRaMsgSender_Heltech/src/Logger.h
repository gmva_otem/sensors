/*
 * Copyright (c) 2020-2023, CASA, IRISA, Université Bretagne Sud, France.
 *
 */

#ifndef LORAOPP_LOGGER_H
#define LORAOPP_LOGGER_H
#include <inttypes.h>
#include <cstdarg>

typedef enum {
    LOG_LEVEL_NOOUTPUT=0,
    LOG_LEVEL_ERRORS=1,
    LOG_LEVEL_INFOS=2,
    LOG_LEVEL_DEBUG=3,
    LOG_LEVEL_VERBOSE=4
} log_level_t;

#define CR "\r\n"


/**
 * This class is used to print logs. It is mainly used for debug
 * purposes.
 * A unique instance of this class is created and used in LoRaOpp.
 */
class Logger {
private:
    log_level_t level_ = LOG_LEVEL_DEBUG;

public:
    /*
     * default Constructor
     */
    Logger(){} ;

    virtual ~Logger(){};
	
    /** 
     * Initializing, must be called as first.
     * \param void
     * \return void
     *
     */
    virtual void init(log_level_t level, int baud);
	
    /**
     * Output an error message. Output message contains
     * ERROR: followed by original msg
     * Error messages are printed out, at every loglevel
     * except 0 ;-)
     * \param msg format string to output
     * \param ... any number of variables
     * \return void
     */
    virtual void error(const char* msg, ...);
	
    /**
     * Output an info message. Output message contains
     * Info messages are printed out at l
     * loglevels >= LOG_LEVEL_INFOS
     *
     * \param msg format string to output
     * \param ... any number of variables
     * \return void
     */

    virtual void info(const char* msg, ...);
	
    /**
     * Output an debug message. Output message contains
     * Debug messages are printed out at l
     * loglevels >= LOG_LEVEL_DEBUG
     *
     * \param msg format string to output
     * \param ... any number of variables
     * \return void
     */

    virtual void debug(const char* msg, ...);
	
    /**
     * Output an verbose message. Output message contains
     * Debug messages are printed out at l
     * loglevels >= LOG_LEVEL_VERBOSE
     *
     * \param msg format string to output
     * \param ... any number of variables
     * \return void
     */

    virtual void verbose(const char* msg, ...);   

};

extern Logger logger;
#endif
