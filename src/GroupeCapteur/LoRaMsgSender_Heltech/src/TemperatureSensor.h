#ifndef TEMPERATURE_SENSOR_H
#define TEMPERATURE_SENSOR_H

#include <OneWire.h>
#include <DallasTemperature.h>

// Broche GPIO utilisée pour la communication OneWire
#define ONE_WIRE_BUS 12

// Initialisation de l'objet OneWire
extern OneWire oneWire;

// Initialisation de l'objet DallasTemperature
extern DallasTemperature sensors;

void initTemperatureSensor();
float getTemperature();
void test_get_temperature(); 

#endif // TEMPERATURE_SENSOR_H
