#ifndef TPL5110_MANAGER_H
#define TPL5110_MANAGER_H

#include <Arduino.h>

class TPL5110Manager {
public:
  TPL5110Manager(uint8_t donePin);
  void begin();
  void signalDone();

private:
  uint8_t _donePin;
};

#endif
