#include "TemperatureSensor.h"

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

/**
 * Initialise le capteur de température DS18B20
*/
void initTemperatureSensor()
{
    sensors.begin();
}

/**
 * Renvoie un float de la température
*/
float getTemperature() // Ajouter l'implémentation de la fonction
{
    sensors.requestTemperatures(); // Demander la température au capteur DS18B20
    float temperature = sensors.getTempCByIndex(0); // Lire la température du premier capteur trouvé sur le bus OneWire
    return temperature;
}

/*
* Verification de la cohérence de la temperature
*/
void test_get_temperature(void) {
    float actual_temperature = getTemperature();

    // Verifie que la temperature est comprise entre 15 et 30°C
    if (actual_temperature > 30 || actual_temperature < 15) {
        Serial.print("Test temp not ok");
    } else {
        Serial.print("Test temp ok");
    }
}

