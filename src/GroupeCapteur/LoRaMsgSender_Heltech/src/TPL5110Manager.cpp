#include "TPL5110Manager.h"

TPL5110Manager::TPL5110Manager(uint8_t donePin) : _donePin(donePin) {
}

/**
 * Position de départ du timer
*/
void TPL5110Manager::begin() {
  pinMode(_donePin, OUTPUT);
  digitalWrite(_donePin, LOW);
}

/**
 * Arret de l'alimentation du micro-contrôleur
*/
void TPL5110Manager::signalDone() {
  digitalWrite(_donePin, HIGH);
  delay(50); // Attends 50 millisecondes
  digitalWrite(_donePin, LOW);
}

