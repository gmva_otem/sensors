#include <Arduino.h>
#include "LoRaManager.h"
#include "TemperatureSensor.h"
#include "RTCManager.h"
#include <stdio.h>
#include <iostream>
#include <string>

void initLoRa() {
  // Initialise le module LoRa
  SPI.begin(5, 19, 27, 18); // SCK, MISO, MOSI, SS
  LoRa.setPins(18, 14, 26); // SS, RST, DIO0
  if (LoRa.begin(BAND, true)) {
    Serial.println("LoRa Initial success!");

    // Configure les paramètres du module LoRa
    LoRa.setFrequency(BAND);
    LoRa.setSpreadingFactor(SPREADING_FACTOR);
    LoRa.setCodingRate4(CODING_RATE_DENOMINATOR);
    LoRa.setSignalBandwidth(SIGNAL_BANDWITH);

  } else {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

/**
 * Envoie le paquet par LoRa.
 */
void sendLoRaMessage(const char *message, size_t messageSize) {
  LoRa.beginPacket();
  LoRa.write((const uint8_t *)message, messageSize);
  LoRa.endPacket();


  Serial.print("Message sent: ");
  for (size_t i = 0; i < messageSize; i++) {
    Serial.print(message[i]);
  }
  Serial.println();
}

/**
 *  Créer le paquet avec les données et appel la méthode sendLoRaMessage pour envoyer le message.
*/
void sendTemp(RTCManager *rtcManager) {
  float temperature = getTemperature();
  int floatId = 1; // A: Flotteur ID
  int sensorId = 1; // B: Capteur ID
  int posixTimestamp = rtcManager->getPosixTimestamp(); // F: POSIX timestamp

  String message = createPaquet(floatId, sensorId, temperature, posixTimestamp);
  sendLoRaMessage(message.c_str(), message.length());
}

/**
 * Passe de décimal à binaire.
*/
String decimalToBinary(int decimal) {
  String binary = "";
  while (decimal > 0) {
    binary = String(decimal % 2) + binary;
    decimal /= 2;
  }
  return binary;
}

/**
 * Ajoute les zero non sinificatif.
*/
String addNumberOfZeroInBeginning(String input, int numZeros) {
  for (int i = 0; i < numZeros; i++) {
    input = "0" + input;
  }
  return input;
}

/**
 * Passe de binaire a Hexadécimal
*/
String bin2hex(const String &binStr) {
  String hexStr = "";
  for (int i = 0; i < binStr.length(); i += 4) {
    int val = 0;
    for (int j = i; j < i + 4; j++) {
      val = val * 2 + (binStr[j] - '0');
    }
    hexStr += String(val, HEX);
  }
  return hexStr;
}

/**
 * Créér le paquet à envoyer en fonctions des attributs de la méthode
*/
String createPaquet(int idFloater, int idSensor, float temp, int timestamp) {
  String binFloater, binSensor, binTempSign, binTempIntPart, binTempFracPart, binTime;

  int a = 5;
  int b = 6;
  int c = 1;
  int d = 6;
  int e = 4;
  int f = 32;
  int g = 2;

  int intPart = (int)temp;
  int fracPart = (temp - (int)temp) * 10;

  String res = "";
  String binString = "";

  binFloater = decimalToBinary(idFloater);
  binSensor = decimalToBinary(idSensor);

  if (temp >= 0) {
    binTempSign = "0";
  } else {
    binTempSign = "1";
  }

  binTempIntPart = decimalToBinary(intPart);
  binTempFracPart = decimalToBinary(fracPart);
  binTime = decimalToBinary(timestamp);

  binFloater = addNumberOfZeroInBeginning(binFloater, a - binFloater.length());
  binSensor = addNumberOfZeroInBeginning(binSensor, b - binSensor.length());
  binTempSign = addNumberOfZeroInBeginning(binTempSign, c - binTempSign.length());
  binTempIntPart = addNumberOfZeroInBeginning(binTempIntPart, d - binTempIntPart.length());
  binTempFracPart = addNumberOfZeroInBeginning(binTempFracPart, e - binTempFracPart.length());
  binTime = addNumberOfZeroInBeginning(binTime, f - binTime.length());

  binString += binFloater;
  binString += binSensor;
  binString += binTempSign;
  binString += binTempIntPart;
  binString += binTempFracPart;
  binString += binTime;
  binString += "00";

  res = bin2hex(binString);
  return res;
}

//*****************************************************************//

void testLora() {
  testDecToBin();
  testBinToHex();
  testAddZero();
  Serial.print("\n\n");
}

/*
* Test de la fonction de convertion 
* de valeurs binaires en valeurs hexadeciales
*/
void testDecToBin() {
  String res1 = decimalToBinary(12);
  if (res1 == "1100") {
    Serial.print("\nTest DecToBin 1 ok");
  } else {
    Serial.print("\nTest DecToBin 1 not ok");
  };



  String res2 = decimalToBinary(29);
  if (res2 == "11101") {
    Serial.print("\nTest DecToBin 2 ok");
  } else {
    Serial.print("\nTest DecToBin 2 not ok");
  };


  String res3 = decimalToBinary(44);
  if (res3 == "101100") {
    Serial.print("\nTest DecToBin 3 ok");
  } else {
    Serial.print("\nTest DecToBin 3 not ok");
  };
}

/*
* Test de la fonction de convertion 
* de valeurs decimales en valeurs binaires
*/
void testBinToHex() {
String res1 = bin2hex("1100");
  if (res1 == "c") {
    Serial.print("\nTest BinToHex 1 ok");
  } else {
    Serial.print("\nTest BinToHex 1 not ok");
  };


  String res2 = bin2hex("00011101");
  if (res2 == "1d") {
    Serial.print("\nTest BinToHex 2 ok");
  } else {
    Serial.print("\nTest BinToHex 2 not ok");
  };


  String res3 = bin2hex("00101100");
  if (res3 == "2c") {
    Serial.print("\nTest BinToHex 3 ok");
  } else {
    Serial.print("\nTest BinToHex 3 not ok");
  };
}

/*
* Test de la fonction pour ajouter un nombre de zero 
* à une valeur pour l'encoder sur 8 bits
*/
void testAddZero() {
  //addNumberOfZeroInBeginning
  String res1 = addNumberOfZeroInBeginning("11101", 3);
  if (res1 == "00011101") {
    Serial.print("\nTest addNumberOfZeroInBeginning 1 ok");
  } else {
    Serial.print("\nTest addNumberOfZeroInBeginning 1 not ok");
  };

  String res2 = addNumberOfZeroInBeginning("11101", 6);
  if (res2 == "00000011101") {
    Serial.print("\nTest addNumberOfZeroInBeginning 2 ok");
  } else {
    Serial.print("\nTest addNumberOfZeroInBeginning 2 not ok");
  };

  String res3 = addNumberOfZeroInBeginning("11101", 0);
  if (res3 == "11101") {
    Serial.print("\nTest addNumberOfZeroInBeginning 3 ok");
  } else {
    Serial.print("\nTest addNumberOfZeroInBeginning 3 not ok");
  };
}
