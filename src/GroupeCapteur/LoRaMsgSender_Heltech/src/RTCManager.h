#ifndef RTCManager_h
#define RTCManager_h

#include <Arduino.h>
#include <RTClib.h>
#include <Wire.h>

class RTCManager {
public:
  RTCManager();
  void begin();
  DateTime now();
  String getDateTimeString();
  uint32_t getPosixTimestamp();
  void testRtcManager();
  void test_begin();
  void test_getDateTimeString();
  void test_getPosixTimestamp();

private:
  RTC_DS3231 rtc;
};

#endif
