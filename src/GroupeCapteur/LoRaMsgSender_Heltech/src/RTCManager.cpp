#include "RTCManager.h"
#include <iostream>

RTC_DS3231 rtc;

RTCManager::RTCManager() {}

/**
 * Initialisation de l'horloge RTC
*/
void RTCManager::begin() {
  //Wire.begin(21, 22); // SDA: GPIO21, SCL: GPIO22

  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, setting the time!");
    // Set the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
}

/**
 * Renvoie la date actuelle sous un objet DateTime
*/
DateTime RTCManager::now() {
  return rtc.now();
}

/**
 * Renvoie un String avec la date et l'heure sous la forme : YYYY/MM/JJ DD:MM:SS
*/
String RTCManager::getDateTimeString() {
  DateTime now = rtc.now();
  char dateTimeStr[20];
  sprintf(dateTimeStr, "%04d/%02d/%02d %02d:%02d:%02d", now.year(), 
    now.month(), now.day(), now.hour(), now.minute(), now.second());
  
  return String(dateTimeStr);
}

/**
 * Renvoie la date et l'heure au format Posix
*/
uint32_t RTCManager::getPosixTimestamp() {
  DateTime now = rtc.now();
  return now.unixtime();  
}


//*******************************************************************//

void RTCManager::testRtcManager() {
  this->test_begin();
  this->test_getDateTimeString();
  this->test_getPosixTimestamp();
  Serial.print("\n\n");
}

void RTCManager::test_begin() {
    this->begin();
    std::cout << "Test begin(): Vérifiez si l'initialisation s'est déroulée correctement" << std::endl;
}

void RTCManager::test_getDateTimeString() {
    String dateTime = this->getDateTimeString();
    std::cout << "Test getDateTimeString(): " << dateTime.c_str() << std::endl;
}

void RTCManager::test_getPosixTimestamp() {
    uint32_t posixTimestamp = this->getPosixTimestamp();
    std::cout << "Test getPosixTimestamp(): " << posixTimestamp << std::endl;
}
