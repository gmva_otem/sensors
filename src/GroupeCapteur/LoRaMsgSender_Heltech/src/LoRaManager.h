#ifndef LORA_MANAGER_H
#define LORA_MANAGER_H

#include "heltec.h"
#include "TemperatureSensor.h"
#include "RTCManager.h"

// LoRa parameters
#define BAND 868E6
#define TX_POWER 17
#define SPREADING_FACTOR 7
#define CODING_RATE_DENOMINATOR 5
#define SIGNAL_BANDWITH 125E3

void initLoRa();
void testLora();
void testDecToBin();
void testBinToHex();
void testAddZero();
void sendTemp(RTCManager *rtcManager);
String decimalToBinary(int decimal);
String addNumberOfZeroInBeginning(String input, int numZeros);
String bin2hex(const String &binStr);
String removeLeadingZeros(const String& val);
String createPaquet(int idFloater, int idSensor, float temp, int timestamp);
void sendLoRaMessage(const uint8_t *message, size_t messageSize);
#endif // LORA_MANAGER_H
