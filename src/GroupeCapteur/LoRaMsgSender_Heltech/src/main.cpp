/*
 * Copyright (c) 2020-2023, CASA, IRISA, Université Bretagne Sud, France.
 *
 */

#include <Arduino.h>
#include "heltec.h"
#include <cstring>
#include <Wire.h>
#include "RTClib.h"
#include "RTCManager.h"
#include "TemperatureSensor.h"
#include "LoRaManager.h"
#include "TPL5110Manager.h"
#include <lmic.h>
#include <hal/hal.h>


// Horloge RTC
RTCManager rtcManager;

// Timer
TPL5110Manager tpl5110Manager(13); // Initialise le TPL5110Manager avec le pin 13


const uint8_t* buffer = (uint8_t*) "Hello";
size_t size = 5;
//------------------------------------------------------------------------------
// Setup function
//------------------------------------------------------------------------------
void setup()
{
    Serial.begin(115200);
    
    // Initialise le capteur de température
    initTemperatureSensor(); 
    delay(1000);

    // Initialise la communication LoRa
    initLoRa(); 
    delay(1000);

    // Initialise le module RTC
    rtcManager.begin();
    delay(1000);

    // Timer
    tpl5110Manager.begin();
    delay(1000);


    test_get_temperature();
    delay(1000);

    testLora();
    delay(1000);

    rtcManager.testRtcManager();
    delay(1000);

}


//------------------------------------------------------------------------------
// Loop function
//------------------------------------------------------------------------------

void loop()
{
    
    // Début partie facultative pour affichager la température dans le port série

    // Print la date
    Serial.println();
    Serial.print(rtcManager.getDateTimeString());

    Serial.print(" : ");

    // Print la température
    Serial.print(getTemperature());
    Serial.println(" C");
    delay(1000);
    // Fin de partie facultative

    // Envoyer la température avec LoRa
    sendTemp(&rtcManager);

    // Timer 
    tpl5110Manager.signalDone();


    //Serial.println();
};
