/*
 * Copyright (c) 2020-2023, CASA, IRISA, Université Bretagne Sud, France.
 *
 */
#include "Logger.h"
#include "Arduino.h"

extern "C" {
    int _write(int fd, char *ptr, int len) {
    (void) fd;
    return Serial.write(ptr, len);
    }
}

void Logger::init(log_level_t level, int baud){
    this->level_ = level;    
    Serial.begin(9600);
}

void Logger::error(const char* msg, ...){
    if (LOG_LEVEL_ERRORS <= this->level_) {   
        va_list args;
        va_start(args, msg);
		printf((char*)"Error :");
		vprintf(msg,args);
		va_end(args);
        fflush(stdout); 
    }
}


void Logger::info(const char* msg, ...){
    if (LOG_LEVEL_INFOS <= this->level_) {
        va_list args;
        va_start(args, msg);
		printf((char*)"Info: ");
		vprintf(msg,args);
		va_end(args);
        fflush(stdout); 
    }
}

void Logger::debug(const char* msg, ...){
    if (LOG_LEVEL_DEBUG <= this->level_) {
        va_list args;
        va_start(args, msg);
		printf((char*)"Debug: ");
		vprintf(msg,args);
		va_end(args);
        fflush(stdout); 
    }
}


void Logger::verbose(const char* msg, ...){
    if (LOG_LEVEL_VERBOSE <= this->level_) {
        va_list args;
        va_start(args, msg);
		printf((char*)"Verbose: ");
        vprintf(msg,args);
		va_end(args);
        fflush(stdout); 
    }
}
 
 Logger logger = Logger();
