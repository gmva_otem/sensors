/*
#ifndef LORANODE_H
#define LORANODE_H

#include <Arduino.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <U8x8lib.h>

constexpr u1_t NWKSKEY[16] = { 0xCB, 0x46, 0x63, 0xCD, 0xDD, 0x22, 0x8B, 0x16, 0x1E, 0xD3, 0x6A, 0x1E, 0xB9, 0x98, 0x2C, 0x76 };
constexpr u1_t APPSKEY[16] = { 0xBE, 0x06, 0x39, 0x85, 0x83, 0xC3, 0xB8, 0x46, 0xCF, 0x91, 0xB0, 0x30, 0x33, 0x64, 0x49, 0x37 };
constexpr u4_t DEVADDR = 0x26011BC6;

extern U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8;
extern const lmic_pinmap lmic_pins;
extern int buttonPin;
extern int ledPin;
extern int boardLED;
extern int lastState;
extern osjob_t sendjob;
extern uint8_t mydata[];

void os_getArtEui(u1_t* buf);
void os_getDevEui(u1_t* buf);
void os_getDevKey(u1_t* buf);
void onEvent(ev_t ev);
void ledFLash(int flashes);
void do_send(osjob_t* j);
void loraNodeSetup();
void loraNodeLoop();

#endif
*/