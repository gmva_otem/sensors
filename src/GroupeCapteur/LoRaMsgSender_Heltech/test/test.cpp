#include <Arduino.h>
#include <unity.h>
#include <TemperatureSensor.h>
// Test de TemperatureSensor
void test_get_temperature(void) {
    float actual_temperature = getTemperature();
    
    TEST_ASSERT_FLOAT_WITHIN(20.0, 5.0, actual_temperature); // Vérifie que la température est supérieure ou égale à 5.0
    TEST_ASSERT_FLOAT_WITHIN(20.0, 35.0, actual_temperature); // Vérifie que la température est inférieure ou égale à 35.0
}



void setup() {
    /*
    delay(2000); // Attendre un peu pour le démarrage du port série
    UNITY_BEGIN();

    // Exécutez tous les tests ici
    RUN_TEST(test_get_temperature);

    // Ajoutez d'autres tests à exécuter ici

    UNITY_END();
    */
}

void loop() {
    // Ne rien mettre ici
}

int main (int argc, char **argv) {
    UNITY_BEGIN();
    RUN_TEST(test_get_temperature);
    UNITY_END();
}