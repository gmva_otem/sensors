#ifndef JSON_H
#define JSON_H

typedef struct json_obj
{
    char *key;
    float value;
    struct json_obj *next;
} json_obj;

json_obj *create_json_obj(char *key, float value);

void add_json_obj(json_obj *json, char *key, float value);

char *json_obj_to_string(json_obj *json);

#endif