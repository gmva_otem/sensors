# EndNode
This folder contains the code that allows the stm32 micro controller to send data using the LoRaWan protocol.
The developpers are Largillet Thomas (Fozl or Mosthal on github), MARGHICH Hamza (Hamza marghich or som-gif on github) and JOLLY-JEHENNE Léo.

## Getting started
To start, open the ".project" file with STM32CubeIde. The part of the code we are interested in is in the file "main.c" in the path "./Core/Src".

## Configuration file
There is a configuration file named "EndNodeConfig.h" in the path "./Core/Inc". It allows to change some variables used in the code. Everything is detailed there

## Documentation
A documentation of the code is available and will comes with the project
