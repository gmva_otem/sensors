################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/littlefs/bd/lfs_filebd.c \
../Middlewares/littlefs/bd/lfs_rambd.c \
../Middlewares/littlefs/bd/lfs_testbd.c 

OBJS += \
./Middlewares/littlefs/bd/lfs_filebd.o \
./Middlewares/littlefs/bd/lfs_rambd.o \
./Middlewares/littlefs/bd/lfs_testbd.o 

C_DEPS += \
./Middlewares/littlefs/bd/lfs_filebd.d \
./Middlewares/littlefs/bd/lfs_rambd.d \
./Middlewares/littlefs/bd/lfs_testbd.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/littlefs/bd/%.o Middlewares/littlefs/bd/%.su: ../Middlewares/littlefs/bd/%.c Middlewares/littlefs/bd/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32WLE5xx -DDEBUG -c -I../Core/Inc -I../../EndNode/Drivers/STM32WLxx_HAL_Driver -I../../EndNode/Drivers/STM32WLxx_HAL_Driver/Inc -I../../EndNode/Drivers/STM32WLxx_HAL_Driver/Inc/Legacy -I../../EndNode/Drivers/CMSIS/Device/ST/STM32WLxx/Include -I../../EndNode/Drivers/CMSIS/Include -I../../EndNode/Middlewares/FreeRTOS/Source/include -I../../EndNode/Middlewares/FreeRTOS/Source/portable/GCC/ARM_CM3 -I../../EndNode/Middlewares/LoRaWAN/Crypto -I../../EndNode/Middlewares/LoRaWAN/LmHandler -I../../EndNode/Middlewares/LoRaWAN/Mac -I../../EndNode/Middlewares/LoRaWAN/Utilities -I../../EndNode/Middlewares/SubGHz_Phy/stm32_radio_driver -I../../EndNode/Middlewares/SubGHz_Phy -I../../EndNode/Drivers/BSP/STM32WLxx_LoRa_E5_mini -I../../EndNode/Utilities/lpm/tiny_lpm -I../../EndNode/Utilities/misc -I../../EndNode/Utilities/sequencer -I../../EndNode/Utilities/timer -I../../EndNode/Utilities/trace/adv_trace -I../../EndNode/Middlewares/LoRaWAN/Mac/Region -I../../EndNode/Middlewares/LoRaWAN/LmHandler/Packages -I../../EndNode/Middlewares/FreeRTOS/Source/CMSIS_RTOS -I../../EndNode/Middlewares/littlefs -I../../EndNode/Middlewares/Crypto -I../../EndNode/LoRaWAN/App -I../../EndNode/LoRaWAN/Target -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Middlewares-2f-littlefs-2f-bd

clean-Middlewares-2f-littlefs-2f-bd:
	-$(RM) ./Middlewares/littlefs/bd/lfs_filebd.d ./Middlewares/littlefs/bd/lfs_filebd.o ./Middlewares/littlefs/bd/lfs_filebd.su ./Middlewares/littlefs/bd/lfs_rambd.d ./Middlewares/littlefs/bd/lfs_rambd.o ./Middlewares/littlefs/bd/lfs_rambd.su ./Middlewares/littlefs/bd/lfs_testbd.d ./Middlewares/littlefs/bd/lfs_testbd.o ./Middlewares/littlefs/bd/lfs_testbd.su

.PHONY: clean-Middlewares-2f-littlefs-2f-bd

