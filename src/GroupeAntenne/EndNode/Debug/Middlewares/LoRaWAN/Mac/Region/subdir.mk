################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/LoRaWAN/Mac/Region/Region.c \
../Middlewares/LoRaWAN/Mac/Region/RegionAS923.c \
../Middlewares/LoRaWAN/Mac/Region/RegionAU915.c \
../Middlewares/LoRaWAN/Mac/Region/RegionBaseUS.c \
../Middlewares/LoRaWAN/Mac/Region/RegionCN470.c \
../Middlewares/LoRaWAN/Mac/Region/RegionCN779.c \
../Middlewares/LoRaWAN/Mac/Region/RegionCommon.c \
../Middlewares/LoRaWAN/Mac/Region/RegionEU433.c \
../Middlewares/LoRaWAN/Mac/Region/RegionEU868.c \
../Middlewares/LoRaWAN/Mac/Region/RegionIN865.c \
../Middlewares/LoRaWAN/Mac/Region/RegionKR920.c \
../Middlewares/LoRaWAN/Mac/Region/RegionRU864.c \
../Middlewares/LoRaWAN/Mac/Region/RegionUS915.c 

OBJS += \
./Middlewares/LoRaWAN/Mac/Region/Region.o \
./Middlewares/LoRaWAN/Mac/Region/RegionAS923.o \
./Middlewares/LoRaWAN/Mac/Region/RegionAU915.o \
./Middlewares/LoRaWAN/Mac/Region/RegionBaseUS.o \
./Middlewares/LoRaWAN/Mac/Region/RegionCN470.o \
./Middlewares/LoRaWAN/Mac/Region/RegionCN779.o \
./Middlewares/LoRaWAN/Mac/Region/RegionCommon.o \
./Middlewares/LoRaWAN/Mac/Region/RegionEU433.o \
./Middlewares/LoRaWAN/Mac/Region/RegionEU868.o \
./Middlewares/LoRaWAN/Mac/Region/RegionIN865.o \
./Middlewares/LoRaWAN/Mac/Region/RegionKR920.o \
./Middlewares/LoRaWAN/Mac/Region/RegionRU864.o \
./Middlewares/LoRaWAN/Mac/Region/RegionUS915.o 

C_DEPS += \
./Middlewares/LoRaWAN/Mac/Region/Region.d \
./Middlewares/LoRaWAN/Mac/Region/RegionAS923.d \
./Middlewares/LoRaWAN/Mac/Region/RegionAU915.d \
./Middlewares/LoRaWAN/Mac/Region/RegionBaseUS.d \
./Middlewares/LoRaWAN/Mac/Region/RegionCN470.d \
./Middlewares/LoRaWAN/Mac/Region/RegionCN779.d \
./Middlewares/LoRaWAN/Mac/Region/RegionCommon.d \
./Middlewares/LoRaWAN/Mac/Region/RegionEU433.d \
./Middlewares/LoRaWAN/Mac/Region/RegionEU868.d \
./Middlewares/LoRaWAN/Mac/Region/RegionIN865.d \
./Middlewares/LoRaWAN/Mac/Region/RegionKR920.d \
./Middlewares/LoRaWAN/Mac/Region/RegionRU864.d \
./Middlewares/LoRaWAN/Mac/Region/RegionUS915.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/LoRaWAN/Mac/Region/%.o Middlewares/LoRaWAN/Mac/Region/%.su: ../Middlewares/LoRaWAN/Mac/Region/%.c Middlewares/LoRaWAN/Mac/Region/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32WLE5xx -DDEBUG -c -I../Core/Inc -I../../EndNode/Drivers/STM32WLxx_HAL_Driver -I../../EndNode/Drivers/STM32WLxx_HAL_Driver/Inc -I../../EndNode/Drivers/STM32WLxx_HAL_Driver/Inc/Legacy -I../../EndNode/Drivers/CMSIS/Device/ST/STM32WLxx/Include -I../../EndNode/Drivers/CMSIS/Include -I../../EndNode/Middlewares/FreeRTOS/Source/include -I../../EndNode/Middlewares/FreeRTOS/Source/portable/GCC/ARM_CM3 -I../../EndNode/Middlewares/LoRaWAN/Crypto -I../../EndNode/Middlewares/LoRaWAN/LmHandler -I../../EndNode/Middlewares/LoRaWAN/Mac -I../../EndNode/Middlewares/LoRaWAN/Utilities -I../../EndNode/Middlewares/SubGHz_Phy/stm32_radio_driver -I../../EndNode/Middlewares/SubGHz_Phy -I../../EndNode/Drivers/BSP/STM32WLxx_LoRa_E5_mini -I../../EndNode/Utilities/lpm/tiny_lpm -I../../EndNode/Utilities/misc -I../../EndNode/Utilities/sequencer -I../../EndNode/Utilities/timer -I../../EndNode/Utilities/trace/adv_trace -I../../EndNode/Middlewares/LoRaWAN/Mac/Region -I../../EndNode/Middlewares/LoRaWAN/LmHandler/Packages -I../../EndNode/Middlewares/FreeRTOS/Source/CMSIS_RTOS -I../../EndNode/Middlewares/littlefs -I../../EndNode/Middlewares/Crypto -I../../EndNode/LoRaWAN/App -I../../EndNode/LoRaWAN/Target -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Middlewares-2f-LoRaWAN-2f-Mac-2f-Region

clean-Middlewares-2f-LoRaWAN-2f-Mac-2f-Region:
	-$(RM) ./Middlewares/LoRaWAN/Mac/Region/Region.d ./Middlewares/LoRaWAN/Mac/Region/Region.o ./Middlewares/LoRaWAN/Mac/Region/Region.su ./Middlewares/LoRaWAN/Mac/Region/RegionAS923.d ./Middlewares/LoRaWAN/Mac/Region/RegionAS923.o ./Middlewares/LoRaWAN/Mac/Region/RegionAS923.su ./Middlewares/LoRaWAN/Mac/Region/RegionAU915.d ./Middlewares/LoRaWAN/Mac/Region/RegionAU915.o ./Middlewares/LoRaWAN/Mac/Region/RegionAU915.su ./Middlewares/LoRaWAN/Mac/Region/RegionBaseUS.d ./Middlewares/LoRaWAN/Mac/Region/RegionBaseUS.o ./Middlewares/LoRaWAN/Mac/Region/RegionBaseUS.su ./Middlewares/LoRaWAN/Mac/Region/RegionCN470.d ./Middlewares/LoRaWAN/Mac/Region/RegionCN470.o ./Middlewares/LoRaWAN/Mac/Region/RegionCN470.su ./Middlewares/LoRaWAN/Mac/Region/RegionCN779.d ./Middlewares/LoRaWAN/Mac/Region/RegionCN779.o ./Middlewares/LoRaWAN/Mac/Region/RegionCN779.su ./Middlewares/LoRaWAN/Mac/Region/RegionCommon.d ./Middlewares/LoRaWAN/Mac/Region/RegionCommon.o ./Middlewares/LoRaWAN/Mac/Region/RegionCommon.su ./Middlewares/LoRaWAN/Mac/Region/RegionEU433.d ./Middlewares/LoRaWAN/Mac/Region/RegionEU433.o ./Middlewares/LoRaWAN/Mac/Region/RegionEU433.su ./Middlewares/LoRaWAN/Mac/Region/RegionEU868.d ./Middlewares/LoRaWAN/Mac/Region/RegionEU868.o ./Middlewares/LoRaWAN/Mac/Region/RegionEU868.su ./Middlewares/LoRaWAN/Mac/Region/RegionIN865.d ./Middlewares/LoRaWAN/Mac/Region/RegionIN865.o ./Middlewares/LoRaWAN/Mac/Region/RegionIN865.su ./Middlewares/LoRaWAN/Mac/Region/RegionKR920.d ./Middlewares/LoRaWAN/Mac/Region/RegionKR920.o ./Middlewares/LoRaWAN/Mac/Region/RegionKR920.su ./Middlewares/LoRaWAN/Mac/Region/RegionRU864.d ./Middlewares/LoRaWAN/Mac/Region/RegionRU864.o ./Middlewares/LoRaWAN/Mac/Region/RegionRU864.su ./Middlewares/LoRaWAN/Mac/Region/RegionUS915.d ./Middlewares/LoRaWAN/Mac/Region/RegionUS915.o ./Middlewares/LoRaWAN/Mac/Region/RegionUS915.su

.PHONY: clean-Middlewares-2f-LoRaWAN-2f-Mac-2f-Region

