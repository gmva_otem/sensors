################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/LoRaWAN/LmHandler/Packages/LmhpClockSync.c \
../Middlewares/LoRaWAN/LmHandler/Packages/LmhpCompliance.c \
../Middlewares/LoRaWAN/LmHandler/Packages/LmhpFirmwareManagement.c \
../Middlewares/LoRaWAN/LmHandler/Packages/LmhpRemoteMcastSetup.c 

OBJS += \
./Middlewares/LoRaWAN/LmHandler/Packages/LmhpClockSync.o \
./Middlewares/LoRaWAN/LmHandler/Packages/LmhpCompliance.o \
./Middlewares/LoRaWAN/LmHandler/Packages/LmhpFirmwareManagement.o \
./Middlewares/LoRaWAN/LmHandler/Packages/LmhpRemoteMcastSetup.o 

C_DEPS += \
./Middlewares/LoRaWAN/LmHandler/Packages/LmhpClockSync.d \
./Middlewares/LoRaWAN/LmHandler/Packages/LmhpCompliance.d \
./Middlewares/LoRaWAN/LmHandler/Packages/LmhpFirmwareManagement.d \
./Middlewares/LoRaWAN/LmHandler/Packages/LmhpRemoteMcastSetup.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/LoRaWAN/LmHandler/Packages/%.o Middlewares/LoRaWAN/LmHandler/Packages/%.su: ../Middlewares/LoRaWAN/LmHandler/Packages/%.c Middlewares/LoRaWAN/LmHandler/Packages/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32WLE5xx -DDEBUG -c -I../Core/Inc -I../../EndNode/Drivers/STM32WLxx_HAL_Driver -I../../EndNode/Drivers/STM32WLxx_HAL_Driver/Inc -I../../EndNode/Drivers/STM32WLxx_HAL_Driver/Inc/Legacy -I../../EndNode/Drivers/CMSIS/Device/ST/STM32WLxx/Include -I../../EndNode/Drivers/CMSIS/Include -I../../EndNode/Middlewares/FreeRTOS/Source/include -I../../EndNode/Middlewares/FreeRTOS/Source/portable/GCC/ARM_CM3 -I../../EndNode/Middlewares/LoRaWAN/Crypto -I../../EndNode/Middlewares/LoRaWAN/LmHandler -I../../EndNode/Middlewares/LoRaWAN/Mac -I../../EndNode/Middlewares/LoRaWAN/Utilities -I../../EndNode/Middlewares/SubGHz_Phy/stm32_radio_driver -I../../EndNode/Middlewares/SubGHz_Phy -I../../EndNode/Drivers/BSP/STM32WLxx_LoRa_E5_mini -I../../EndNode/Utilities/lpm/tiny_lpm -I../../EndNode/Utilities/misc -I../../EndNode/Utilities/sequencer -I../../EndNode/Utilities/timer -I../../EndNode/Utilities/trace/adv_trace -I../../EndNode/Middlewares/LoRaWAN/Mac/Region -I../../EndNode/Middlewares/LoRaWAN/LmHandler/Packages -I../../EndNode/Middlewares/FreeRTOS/Source/CMSIS_RTOS -I../../EndNode/Middlewares/littlefs -I../../EndNode/Middlewares/Crypto -I../../EndNode/LoRaWAN/App -I../../EndNode/LoRaWAN/Target -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Middlewares-2f-LoRaWAN-2f-LmHandler-2f-Packages

clean-Middlewares-2f-LoRaWAN-2f-LmHandler-2f-Packages:
	-$(RM) ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpClockSync.d ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpClockSync.o ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpClockSync.su ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpCompliance.d ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpCompliance.o ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpCompliance.su ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpFirmwareManagement.d ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpFirmwareManagement.o ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpFirmwareManagement.su ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpRemoteMcastSetup.d ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpRemoteMcastSetup.o ./Middlewares/LoRaWAN/LmHandler/Packages/LmhpRemoteMcastSetup.su

.PHONY: clean-Middlewares-2f-LoRaWAN-2f-LmHandler-2f-Packages

