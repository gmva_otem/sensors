################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/FreeRTOS/Source/croutine.c \
../Middlewares/FreeRTOS/Source/event_groups.c \
../Middlewares/FreeRTOS/Source/list.c \
../Middlewares/FreeRTOS/Source/queue.c \
../Middlewares/FreeRTOS/Source/stream_buffer.c \
../Middlewares/FreeRTOS/Source/tasks.c \
../Middlewares/FreeRTOS/Source/timers.c 

OBJS += \
./Middlewares/FreeRTOS/Source/croutine.o \
./Middlewares/FreeRTOS/Source/event_groups.o \
./Middlewares/FreeRTOS/Source/list.o \
./Middlewares/FreeRTOS/Source/queue.o \
./Middlewares/FreeRTOS/Source/stream_buffer.o \
./Middlewares/FreeRTOS/Source/tasks.o \
./Middlewares/FreeRTOS/Source/timers.o 

C_DEPS += \
./Middlewares/FreeRTOS/Source/croutine.d \
./Middlewares/FreeRTOS/Source/event_groups.d \
./Middlewares/FreeRTOS/Source/list.d \
./Middlewares/FreeRTOS/Source/queue.d \
./Middlewares/FreeRTOS/Source/stream_buffer.d \
./Middlewares/FreeRTOS/Source/tasks.d \
./Middlewares/FreeRTOS/Source/timers.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/FreeRTOS/Source/%.o Middlewares/FreeRTOS/Source/%.su: ../Middlewares/FreeRTOS/Source/%.c Middlewares/FreeRTOS/Source/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32WLE5xx -DDEBUG -c -I../Core/Inc -I../../EndNode/Drivers/STM32WLxx_HAL_Driver -I../../EndNode/Drivers/STM32WLxx_HAL_Driver/Inc -I../../EndNode/Drivers/STM32WLxx_HAL_Driver/Inc/Legacy -I../../EndNode/Drivers/CMSIS/Device/ST/STM32WLxx/Include -I../../EndNode/Drivers/CMSIS/Include -I../../EndNode/Middlewares/FreeRTOS/Source/include -I../../EndNode/Middlewares/FreeRTOS/Source/portable/GCC/ARM_CM3 -I../../EndNode/Middlewares/LoRaWAN/Crypto -I../../EndNode/Middlewares/LoRaWAN/LmHandler -I../../EndNode/Middlewares/LoRaWAN/Mac -I../../EndNode/Middlewares/LoRaWAN/Utilities -I../../EndNode/Middlewares/SubGHz_Phy/stm32_radio_driver -I../../EndNode/Middlewares/SubGHz_Phy -I../../EndNode/Drivers/BSP/STM32WLxx_LoRa_E5_mini -I../../EndNode/Utilities/lpm/tiny_lpm -I../../EndNode/Utilities/misc -I../../EndNode/Utilities/sequencer -I../../EndNode/Utilities/timer -I../../EndNode/Utilities/trace/adv_trace -I../../EndNode/Middlewares/LoRaWAN/Mac/Region -I../../EndNode/Middlewares/LoRaWAN/LmHandler/Packages -I../../EndNode/Middlewares/FreeRTOS/Source/CMSIS_RTOS -I../../EndNode/Middlewares/littlefs -I../../EndNode/Middlewares/Crypto -I../../EndNode/LoRaWAN/App -I../../EndNode/LoRaWAN/Target -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Middlewares-2f-FreeRTOS-2f-Source

clean-Middlewares-2f-FreeRTOS-2f-Source:
	-$(RM) ./Middlewares/FreeRTOS/Source/croutine.d ./Middlewares/FreeRTOS/Source/croutine.o ./Middlewares/FreeRTOS/Source/croutine.su ./Middlewares/FreeRTOS/Source/event_groups.d ./Middlewares/FreeRTOS/Source/event_groups.o ./Middlewares/FreeRTOS/Source/event_groups.su ./Middlewares/FreeRTOS/Source/list.d ./Middlewares/FreeRTOS/Source/list.o ./Middlewares/FreeRTOS/Source/list.su ./Middlewares/FreeRTOS/Source/queue.d ./Middlewares/FreeRTOS/Source/queue.o ./Middlewares/FreeRTOS/Source/queue.su ./Middlewares/FreeRTOS/Source/stream_buffer.d ./Middlewares/FreeRTOS/Source/stream_buffer.o ./Middlewares/FreeRTOS/Source/stream_buffer.su ./Middlewares/FreeRTOS/Source/tasks.d ./Middlewares/FreeRTOS/Source/tasks.o ./Middlewares/FreeRTOS/Source/tasks.su ./Middlewares/FreeRTOS/Source/timers.d ./Middlewares/FreeRTOS/Source/timers.o ./Middlewares/FreeRTOS/Source/timers.su

.PHONY: clean-Middlewares-2f-FreeRTOS-2f-Source

