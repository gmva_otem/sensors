################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/SubGHz_Phy/stm32_radio_driver/radio.c \
../Middlewares/SubGHz_Phy/stm32_radio_driver/radio_driver.c \
../Middlewares/SubGHz_Phy/stm32_radio_driver/radio_fw.c 

OBJS += \
./Middlewares/SubGHz_Phy/stm32_radio_driver/radio.o \
./Middlewares/SubGHz_Phy/stm32_radio_driver/radio_driver.o \
./Middlewares/SubGHz_Phy/stm32_radio_driver/radio_fw.o 

C_DEPS += \
./Middlewares/SubGHz_Phy/stm32_radio_driver/radio.d \
./Middlewares/SubGHz_Phy/stm32_radio_driver/radio_driver.d \
./Middlewares/SubGHz_Phy/stm32_radio_driver/radio_fw.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/SubGHz_Phy/stm32_radio_driver/%.o Middlewares/SubGHz_Phy/stm32_radio_driver/%.su: ../Middlewares/SubGHz_Phy/stm32_radio_driver/%.c Middlewares/SubGHz_Phy/stm32_radio_driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32WLE5xx -DDEBUG -c -I../Core/Inc -I../../EndNode/Drivers/STM32WLxx_HAL_Driver -I../../EndNode/Drivers/STM32WLxx_HAL_Driver/Inc -I../../EndNode/Drivers/STM32WLxx_HAL_Driver/Inc/Legacy -I../../EndNode/Drivers/CMSIS/Device/ST/STM32WLxx/Include -I../../EndNode/Drivers/CMSIS/Include -I../../EndNode/Middlewares/FreeRTOS/Source/include -I../../EndNode/Middlewares/FreeRTOS/Source/portable/GCC/ARM_CM3 -I../../EndNode/Middlewares/LoRaWAN/Crypto -I../../EndNode/Middlewares/LoRaWAN/LmHandler -I../../EndNode/Middlewares/LoRaWAN/Mac -I../../EndNode/Middlewares/LoRaWAN/Utilities -I../../EndNode/Middlewares/SubGHz_Phy/stm32_radio_driver -I../../EndNode/Middlewares/SubGHz_Phy -I../../EndNode/Drivers/BSP/STM32WLxx_LoRa_E5_mini -I../../EndNode/Utilities/lpm/tiny_lpm -I../../EndNode/Utilities/misc -I../../EndNode/Utilities/sequencer -I../../EndNode/Utilities/timer -I../../EndNode/Utilities/trace/adv_trace -I../../EndNode/Middlewares/LoRaWAN/Mac/Region -I../../EndNode/Middlewares/LoRaWAN/LmHandler/Packages -I../../EndNode/Middlewares/FreeRTOS/Source/CMSIS_RTOS -I../../EndNode/Middlewares/littlefs -I../../EndNode/Middlewares/Crypto -I../../EndNode/LoRaWAN/App -I../../EndNode/LoRaWAN/Target -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Middlewares-2f-SubGHz_Phy-2f-stm32_radio_driver

clean-Middlewares-2f-SubGHz_Phy-2f-stm32_radio_driver:
	-$(RM) ./Middlewares/SubGHz_Phy/stm32_radio_driver/radio.d ./Middlewares/SubGHz_Phy/stm32_radio_driver/radio.o ./Middlewares/SubGHz_Phy/stm32_radio_driver/radio.su ./Middlewares/SubGHz_Phy/stm32_radio_driver/radio_driver.d ./Middlewares/SubGHz_Phy/stm32_radio_driver/radio_driver.o ./Middlewares/SubGHz_Phy/stm32_radio_driver/radio_driver.su ./Middlewares/SubGHz_Phy/stm32_radio_driver/radio_fw.d ./Middlewares/SubGHz_Phy/stm32_radio_driver/radio_fw.o ./Middlewares/SubGHz_Phy/stm32_radio_driver/radio_fw.su

.PHONY: clean-Middlewares-2f-SubGHz_Phy-2f-stm32_radio_driver

