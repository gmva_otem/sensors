################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
OBJCOPY_HEX := 
SIZE_OUTPUT := 
OBJDUMP_LIST := 
SU_FILES := 
EXECUTABLES := 
OBJS := 
MAP_FILES := 
S_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Core/Src \
Core/Startup \
Drivers/BSP/STM32WLxx_LoRa_E5_mini \
Drivers/STM32WLxx_HAL_Driver/Src \
LoRaWAN/App \
LoRaWAN/Target \
Middlewares/FreeRTOS/Source/CMSIS_RTOS \
Middlewares/FreeRTOS/Source \
Middlewares/FreeRTOS/Source/portable/GCC/ARM_CM3 \
Middlewares/FreeRTOS/Source/portable/MemMang \
Middlewares/LoRaWAN/Crypto \
Middlewares/LoRaWAN/LmHandler \
Middlewares/LoRaWAN/LmHandler/Packages \
Middlewares/LoRaWAN/Mac \
Middlewares/LoRaWAN/Mac/Region \
Middlewares/LoRaWAN/Utilities \
Middlewares/SubGHz_Phy/stm32_radio_driver \
Middlewares/littlefs/bd \
Middlewares/littlefs \
Utilities/lpm/tiny_lpm \
Utilities/misc \
Utilities/sequencer \
Utilities/timer \
Utilities/trace/adv_trace \

