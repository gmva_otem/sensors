#ifndef INC_ENDNODECONFIG_H_
#define INC_ENDNODECONFIG_H_

/**
 * +----------------------------------+
 * | Conversion table in milliseconds |
 * +-----------------+----------------+
 * | 1 second        |        1000 ms |
 * | 1 minute        |       60000 ms |
 * | 1 hour          |     3600000 ms |
 * | 1 day           |    86400000 ms |
 * +-----------------+----------------+
 */

/**
 * Defines the time between each network connection attempt when sending data.
 * Default: 20000 (20 secondes)
 */
#define JOIN_CYCLE 1000

/**
 * Defines the time between each data sending.
 * Default: 14400000 (4 hours)
 */
#define SEND_CYCLE 10000 // 30 sec

/**
 * Defines the maximum number of connection attempts for a data transmission.
 * Default: 4
 */
#define MAX_RETRY 2

/**
 * Defines the id of the floater.
 */
#define FLOATER_ID 1

#if SEND_CYCLE <= (JOIN_CYCLE * MAX_RETRY)
#error SEND_CYCLE must be greater than the maximum time to join the network.
#endif

#endif
