#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void genTemp(float *tempList, int n)
{
    srand(time(NULL));
    for (int i = 0; i < n; i++)
    {
        // Generate random number between -10 and 30
        // The number is divided by 1 or 2 to get a decimal of .0 or .5
        tempList[i] = rand() % 40 - 10 + (1.0 / (rand() % 2 + 1));
    }
}
