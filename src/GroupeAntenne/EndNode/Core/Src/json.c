#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "json.h"

json_obj *create_json_obj(char *key, float value)
{
    json_obj *json = (json_obj *)malloc(sizeof(json_obj));
    json->key = key;
    json->value = value;
    json->next = NULL;
    return json;
}

void add_json_obj(json_obj *json, char *key, float value)
{
    json_obj *temp = json;
    while (temp->next != NULL)
    {
        temp = temp->next;
    }
    json_obj *new_json = create_json_obj(key, value);
    temp->next = new_json;
}

char *json_obj_to_string(json_obj *json)
{
    json_obj *temp = json;
    char *json_string = (char *)malloc(sizeof(char) * 100);
    strcpy(json_string, "{");
    while (temp != NULL)
    {
        char *key = temp->key;
        float value = temp->value;
        char *value_string = (char *)malloc(sizeof(char) * 10);
        sprintf(value_string, "%f", value);
        strcat(json_string, "\"");
        strcat(json_string, key);
        strcat(json_string, "\"");
        strcat(json_string, ":");
        strcat(json_string, value_string);
        strcat(json_string, ",");
        temp = temp->next;
    }
    json_string[strlen(json_string) - 1] = '}';
    return json_string;
}
