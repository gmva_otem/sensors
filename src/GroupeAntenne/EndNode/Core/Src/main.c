/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <math.h>


#include "main.h"
#include "cmsis_os.h"
#include "app_lorawan.h"
#include "sys_app.h"
#include "LmHandler.h"
#include "lora_at.h"
#include "stm32wlxx_hal.h"

#include "json.h"
#include "EndNodeConfig.h"


LPTIM_HandleTypeDef hlptim1;
osThreadId LED_TaskHandle;
osThreadId LoRaWAN_TaskHandle;
osThreadId SendData_TaskHandle;

CRYP_HandleTypeDef hcryp;
CRYP_ConfigTypeDef Conf;

int attempt;
int currSendCycle;
int currJoinCycle;
int i;
int joined;


void SystemClock_Config(void);
void MX_GPIO_Init(void);
static void MX_LPTIM1_Init(void);
int32_t LED_control(int value);
void StartLoRaWANTask(void const * argument);
void StartLedTask(void const * argument);
void sendData(void);

uint32_t AESKey[4] = {0x01234567, 0x89ABCDEF, 0xFEDCBA98, 0x76543210};
uint32_t AESIV[4] = {0x00010203, 0x04050607, 0x08090A0B, 0x0C0D0E0F};

uint32_t Plaintext[4] = {0x01234567, 0x89ABCDEF, 0x01234567, 0x89ABCDEF};
uint32_t Encryptedtext[4];


/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  MX_GPIO_Init();
  MX_LPTIM1_Init();

  osThreadDef(LED_Task, StartLedTask, osPriorityNormal, 0, 128);
  LED_TaskHandle = osThreadCreate(osThread(LED_Task), NULL);
  osThreadDef(LoRaWAN_Task, StartLoRaWANTask, osPriorityNormal, 0, 1024);
  LoRaWAN_TaskHandle = osThreadCreate(osThread(LoRaWAN_Task), NULL);
  osKernelStart();
  while (1) {}
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure LSE Drive Capability */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Configure the main internal regulator output voltage */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE | RCC_OSCILLATORTYPE_MSI | RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_11;
  RCC_OscInitStruct.LSIDiv = RCC_LSI_DIV1;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig( & RCC_OscInitStruct) != HAL_OK) {
    Error_Handler();
  }
  /** Configure the SYSCLKSource, HCLK, PCLK1 and PCLK2 clocks dividers */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK3 | RCC_CLOCKTYPE_HCLK |
    RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 |
    RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.AHBCLK3Divider = RCC_SYSCLK_DIV1;

  if (HAL_RCC_ClockConfig( & RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
    Error_Handler();
  }
}

/**
 * @brief LPTIM1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_LPTIM1_Init(void) {
  hlptim1.Instance = LPTIM1;
  hlptim1.Init.Clock.Source = LPTIM_CLOCKSOURCE_APBCLOCK_LPOSC;
  hlptim1.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV1;
  hlptim1.Init.Trigger.Source = LPTIM_TRIGSOURCE_SOFTWARE;
  hlptim1.Init.OutputPolarity = LPTIM_OUTPUTPOLARITY_HIGH;
  hlptim1.Init.UpdateMode = LPTIM_UPDATE_IMMEDIATE;
  hlptim1.Init.CounterSource = LPTIM_COUNTERSOURCE_INTERNAL;
  hlptim1.Init.Input1Source = LPTIM_INPUT1SOURCE_GPIO;
  hlptim1.Init.Input2Source = LPTIM_INPUT2SOURCE_GPIO;

  if (HAL_LPTIM_Init( & hlptim1) != HAL_OK) {
    Error_Handler();
  }
}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
void MX_GPIO_Init(void) {
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /* Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);

  /* Configure GPIO pin : PB5 */
  GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, & GPIO_InitStruct);

}


int32_t LED_control(int value) {
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, value);
  return 0;
}

/**
 * @brief method called at the beginning of the program to activate the LED
*/
void StartLedTask(void const * argument) {
  LED_control(1);
  for (;;) {
    LED_control(0);
    osDelay(500);
    LED_control(1);
    osDelay(500);
  }
}

/**
 * @brief method called at the beginning of the program to begin the LoRaWAN tasks
 * @note this methode turns infinitly
*/
void StartLoRaWANTask(void const * argument) {
  MX_LoRaWAN_Init();
  attempt = 0;
  currSendCycle = 0;
  currJoinCycle = 0;
  joined = 0;
  i = 0;

  // AT_Join("1");
  for (;;) {
    if (LmHandlerJoinStatus() == LORAMAC_HANDLER_SET) {
      joined = 1;
    }
    else {
    	joined = 0;
    }

    if (currSendCycle >= SEND_CYCLE) {
      sendData();
    }

    MX_LoRaWAN_Process();
    osDelay(10);
    currSendCycle += 10;
    //APP_LOG(TS_OFF, VLEVEL_M, "10ms passed\r\n");
  }
}


void DisplayAll(uint32_t *data, uint32_t datalength)
{
  uint32_t BufferCounter = 0;
  uint32_t count = 0;
  uint8_t * ptr = (uint8_t *)data;

  for (BufferCounter = 0; BufferCounter < datalength*4; BufferCounter++)
  {
	char buffer[128];
	snprintf(buffer, sizeof(buffer), "[0x%02X]\r\n", *ptr++);
    APP_LOG(TS_OFF, VLEVEL_M, buffer);
    count++;

    if (count == 16)
    {
      count = 0;
      printf("  Block %u \n\r", BufferCounter / 16);
    }
  }
}


/**
 * @brief convert a string to a binary string
 * @param binaire The binary string
 * @param nombre The number to convert
 * @retval None
 * @note The binary string must be freed after use (free(binaire) after use)
*/
void binaryString(char* binaire, int nombre){
    int i = 0;
    while (nombre > 0){
        int reste = nombre % 2;
        binaire[i] = reste + '0';
        nombre /= 2;
        i++;
    }
    binaire[i] = '\0';
    int j = 0;
    int k = strlen(binaire) - 1;
    while (j < k){
        char temp = binaire[j];
        binaire[j] = binaire[k];
        binaire[k] = temp;
        j++;
        k--;
    }

}

/**
 * @brief Convert a binary string to a hexadecimal string
 * @param result The hexadecimal string
 * @param binaire The binary string
 * @retval None
 * @note The hexadecimal string must be freed after use (free(hexadecimal) after use)
*/
void binToHex(char* result, char* binaire){
    while (strlen(binaire) % 4 != 0){
        char* temp = (char*)malloc(sizeof(char) * (strlen(binaire) + 2));
        strcpy(temp, "0");
        strcat(temp, binaire);
        binaire = temp;
    }

    int nb_groupes = strlen(binaire) / 4;
    char** groupes = (char**)malloc(sizeof(char*) * nb_groupes);
    for (int i = 0; i < nb_groupes; i++){
        groupes[i] = (char*)malloc(sizeof(char) * 5);
        strncpy(groupes[i], binaire + i * 4, 4);
        groupes[i][4] = '\0';
    }


    char* hexadecimal = (char*)malloc(sizeof(char) * (nb_groupes + 1));
    hexadecimal[0] = '\0';
    for (int i = 0; i < nb_groupes; i++){
        int valeur = 0;
        for (int j = 0; j < 4; j++){
            valeur += (groupes[i][j] - '0') * (1 << (3 - j));
        }
        char temp[2];
        if (valeur < 10){
            sprintf(temp, "%d", valeur);
        }
        else{
            sprintf(temp, "%c", 'A' + valeur - 10);
        }
        strcat(hexadecimal, temp);
    }
    for (int i = 0; i < nb_groupes; i++){
        free(groupes[i]);
    }

    strcpy(result, hexadecimal);
    free(groupes);
}

/**
 * @brief  Add zeros in the begining of a string
 * @param number string to modify
 * @param length number of zeros to add
 * @retval None
*/
void addZeroInBegining(char* number, int length){
    char* temp = (char*)malloc(sizeof(char) * (length + strlen(number) + 1));
    temp[0] = '\0';
    for (int i = 0; i < length; i++){
        strcat(temp, "0");
    }
    strcat(temp, number);
    strcpy(number, temp);
    free(temp);
}

/**
 * @brief  Generate a packet to be sent to the bridge
 * @param packet variable to store the packet
 * @param idFloater id of the floater
 * @param idSensor id of the sensor
 * @param temp temperature
 * @param timestamp timestamp expressed in seconds since 1st January 1970 (Unix time)
 * @retval None
 * @note The packet is stored in the following binary format :
 *      | idFloater | idSensor | isNeg | entPart | decPart | timestamp | padding |
 *      | 5 bits    | 6 bits   | 1 bit | 6 bits  | 4 bits  | 32 bits   | 2 bits  |
 * @note The packet is sent in hexadecimal format
*/
void generatePacket(char packet[], int idFloater, int idSensor, float temp, int timestamp){

    int idFloaterLen = 5;
    int idSensorLen = 6;
    int isNegLen = 1;
    int entPartLen = 6;
    int decPartLen = 4;
    int timestampLen = 32;
    int paddingLen = 2;

    int entPart = (int)temp;
    int decPart = (int)((temp - entPart) * 10);

    char* binFloater = (char*)malloc(sizeof(char) * 33);
    binaryString(binFloater, idFloater);
    addZeroInBegining(binFloater, idFloaterLen - strlen(binFloater));

    char* binSensor = (char*)malloc(sizeof(char) * 33);
    binaryString(binSensor, idSensor);
    addZeroInBegining(binSensor, idSensorLen - strlen(binSensor));
    printf("%s\n", binSensor);

    char* binIsNeg = (char*)malloc(sizeof(char) * 2);
    binIsNeg[0] = (temp < 0) ? '1' : '0';

    char* binEntPart = (char*)malloc(sizeof(char) * 33);
    binaryString(binEntPart, entPart);
    addZeroInBegining(binEntPart, entPartLen - strlen(binEntPart));

    char* binDecPart = (char*)malloc(sizeof(char) * 33);
    binaryString(binDecPart, decPart);
    addZeroInBegining(binDecPart, decPartLen - strlen(binDecPart));

    char* binTimestamp = (char*)malloc(sizeof(char) * 33);
    binaryString(binTimestamp, timestamp);
    addZeroInBegining(binTimestamp, timestampLen - strlen(binTimestamp));

    strcat(packet, binFloater);

    strcat(packet, binSensor);

    strcat(packet, binIsNeg);
    strcat(packet, binEntPart);
    strcat(packet, binDecPart);
    strcat(packet, binTimestamp);
    strcat(packet, "00\0");

    char* hexPacket = (char*)malloc(sizeof(char) * (strlen(packet) / 4 + 1));
    binToHex(hexPacket, packet);
    strcpy(packet, hexPacket);
    
    free(hexPacket);
    free(binFloater);
    free(binSensor);
    free(binIsNeg);
    free(binEntPart);
    free(binDecPart);
    free(binTimestamp);
}

/**
 * @brief Round a temperature to the nearest 0.5 degree
 * @param temp The temperature to round
 * @return The rounded temperature
*/
float roundHalf(float temp)
{
    int integer_part = (int) temp;
    float decimal_part = temp - integer_part;
    if(decimal_part < 0.25f){
        decimal_part = 0.0f;
    }
    else if(decimal_part >= 0.25f && decimal_part < 0.75f){
        decimal_part = 0.5f;
    }
    else{
        decimal_part = 1.0f;
    }
    return integer_part + decimal_part;
}

/**
 * @brief Generate a list of random temperatures 
 * @param tempList The list that will contain the temperatures
 * @param n The number of temperatures to generate
 * @param min The minimum temperature
 * @param max The maximum temperature
 * @note The temperatures are generated randomly with a precision of 0.5 degrees
*/
void generateRandomTemperatures(float* tempList, int n, float min, float max)
{

    if (tempList == NULL) {
        printf("Erreur d'allocation memoire.\n");
        tempList = NULL;
    }

    srand(time(NULL));
    for (int i = 0; i < n; i++)
    {
        tempList[i] = ((float)rand() / RAND_MAX) * (max - min) + min;
        tempList[i] = tempList[i] + 1.0 / (rand() % 7 + 1);
        tempList[i] = roundHalf(tempList[i]);
    }
}

/**
 * @brief Return a data packet to be sent to the bridge
 * @param i The index of the data packet to be returned
 * @return The data packet
 * @note The data packets have been generated randomly with coherent values
*/
char* genTemp(int i){
	char* data[] = {"1:1:0862D590DADC00","1:1:08631590DB1440","1:1:0862D590DB4C80","1:1:08630190DB84C0","1:1:08630190DBBD00","1:1:0862D590DBF540","1:1:0862D590DC2D80","1:1:08630190DC65C0","1:1:0862D590DC9E00","1:1:0862D590DCD640","1:1:08630190DD0E80","1:1:08631590DD46C0","1:1:08630190DD7F00","1:1:0862D590DDB740","1:1:0862C190DDEF80","1:1:08630190DE27C0","1:1:08631590DE6000","1:1:0862D590DE9840","1:1:08631590DED080","1:1:08631590DF08C0","1:1:0862D590DF4100","1:1:0862D590DF7940","1:1:08630190DFB180","1:1:08630190DFE9C0","1:1:08630190E02200","1:1:08631590E05A40","1:1:08631590E09280","1:1:08630190E0CAC0","1:1:08630190E10300","1:1:0862D590E13B40","1:1:08630190E17380","1:1:08630190E1ABC0","1:1:0862D590E1E400","1:1:08631590E21C40","1:1:0862D590E25480","1:1:08631590E28CC0","1:1:0862C190E2C500","1:1:08630190E2FD40","1:1:08630190E33580","1:1:08630190E36DC0","1:1:08631590E3A600","1:1:08630190E3DE40","1:1:08631590E41680","1:1:08630190E44EC0","1:1:08634190E48700","1:1:08631590E4BF40","1:1:08630190E4F780","1:1:08631590E52FC0","1:1:0862D590E56800","1:1:08630190E5A040","1:1:0862D590E5D880","1:1:08630190E610C0","1:1:0862D590E64900","1:1:08630190E68140","1:1:08630190E6B980","1:1:08630190E6F1C0","1:1:0862D590E72A00","1:1:08631590E76240","1:1:0862D590E79A80","1:1:0862C190E7D2C0","1:1:08631590E80B00","1:1:08630190E84340","1:1:08630190E87B80","1:1:08630190E8B3C0","1:1:0862C190E8EC00","1:1:08630190E92440","1:1:0862C190E95C80","1:1:0862D590E994C0","1:1:08630190E9CD00","1:1:08630190EA0540","1:1:08630190EA3D80","1:1:0862D590EA75C0","1:1:0862D590EAAE00","1:1:08631590EAE640","1:1:08630190EB1E80","1:1:0862D590EB56C0","1:1:08630190EB8F00","1:1:0862D590EBC740","1:1:0862D590EBFF80","1:1:0862D590EC37C0","1:1:08630190EC7000","1:1:08634190ECA840","1:1:08631590ECE080","1:1:08631590ED18C0","1:1:0862D590ED5100","1:1:0862D590ED8940","1:1:08630190EDC180","1:1:0862D590EDF9C0","1:1:08630190EE3200","1:1:0862D590EE6A40","1:1:0862D590EEA280","1:1:08630190EEDAC0","1:1:08630190EF1300","1:1:0862D590EF4B40","1:1:08630190EF8380","1:1:08630190EFBBC0","1:1:08630190EFF400","1:1:0862D590F02C40","1:1:08630190F06480","1:1:08631590F09CC0"};
	return data[i];
}



/**
 * @brief  Send data to bridge
 * @param  None
 * @retval None
 * @note   This function is called every 10 seconds
 * It sends data to bridge (with random generated data for demo purpose)
 * 
*/
void sendData(void) {
	if (currJoinCycle >= JOIN_CYCLE || currJoinCycle == 0 || joined == 1) {
		if (joined == 0) {
		  if (attempt == MAX_RETRY) {
        attempt = 0;
        currSendCycle -= SEND_CYCLE;
        currJoinCycle = 0;
        APP_LOG(TS_OFF, VLEVEL_M, "MAX RETRY\r\n");
        return;
      }
		  else {
        AT_Join("1");
        APP_LOG(TS_OFF, VLEVEL_M, "Joining\r\n");
        attempt++;
        currJoinCycle = 0;
		  }
		}
		else {

      /*
      APP_LOG(TS_OFF, VLEVEL_M, "Packet sent : ");
      APP_LOG(TS_OFF, VLEVEL_M, genTemp(i));
			APP_LOG(TS_OFF, VLEVEL_M, "\r\n");
			AT_Send(genTemp(i));
			i += 1;
      if (i == 100) {
        i = 0;
      }
      */

      int n = 1;
      int idFloater = 1;
      int idSensor = 3;
      float* temps = (float) malloc(sizeof(float) n);
      generateRandomTemperatures(temps, n, 20, 22);
      int timestamp = 1675984884;
      char packet[57] = "";
      generatePacket(packet, idFloater, idSensor, temps[0], timestamp);
      AT_Send(packet);
      free(temps);
      
      currSendCycle -= SEND_CYCLE;
      currJoinCycle = 0;
      return;
    }
  }
  currJoinCycle += 10;
}


/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1) {}
}

#ifdef USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t * file, uint32_t line) {
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  while (1) {}
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
