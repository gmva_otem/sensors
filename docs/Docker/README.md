# Installation et configuration de Docker

## Table des matières

1. [Introduction](#introduction)
2. [Docker Install](#DockerInstall)
3. [Riot et STM32 Config](#RiotConfig)
4. [Tutos Riot & stm32](#Tutos)


## Introduction <a name = introduction></a>

Dans ce dossier, vous allez apprendre à installer Docker sous **Linux** et sous **Windows**. Afin d'avoir une machine virtuelle avec **PlatformIO** ou  **STM32CubeIDE** fonctionnels.

#### Qu'est-ce que Docker ?

Docker est un service qui permet la **contenérisation** d'application, d'environnement, etc... Cela est très utile pour déployer une application sur plusieurs systèmes différents car Docker permet de générer une "image Docker" qui peut être exécuté dans un **conténeur**. 

Concretement, cela signifie que, peu importe l'environnement de développement de départ de votre application, grâce à l'image Docker que vous pourrez générer, vous aurez l'assurance que votre application se comportera de la meme facon sur tous les autres environement grâce à la **contenérisation**. 

Cela vous semble trop beau pour être vrai sans qu'il n'y ai de contre-coup n'est-ce pas? Vous vous imaginez surement qu'en passant par ce genre de "stratagèmes", vous subirez surement une baisse flagrante des performances de votre application. Et bien, détrompez vous puisque la contenérisation n'est que **très peu couteuse en performance**. Ainsi, vous concerverez des performances quasiment similaires par rapport à la même application qui serait executée en natif.

#### Qu'est ce que sont PlatformIO et STM32CubeIDE ?




## Installation de Docker <a name = DockerInstall></a>

Vous retrouverez un guide d'installation de Docker en [français](./INSTALL.fr.md) ou en [anglais](./INSTALL.md). 


## Configuration d'un conténeur avec un DockerFile <a name = RiotConfig></a>

Une fois que vous aurez installé Docker, vous pourrez à l'aide d'un DockerFile, configurer un conténeur afin d'avoir une machine virtuelle prête pour l'utilisation de **PlatformIO** ou de **STM32CubeIDE**. 

Pour cela, vous pouvez vous référer à ces guides d'utilisation :
+ configuration avec **PlatformIO** en [français](./docker_platformIO/UserGuide.fr.md) ou en [anglais](./docker_platformIO/UserGuide.md)
+ configuration avec **STM32CubeIDE** en [français](./docker_stm32cubeIDE/UserGuide.fr.md) ou en [anglais](./docker_stm32cubeIDE/UserGuide.md)


## Comment utilisé PlatformIO et STM32CubeIDE ? <a name = Tutos></a>

Maintenant que votre configuration est terminée, regardez les tutoriels pour  l'utilisation de [PlatformIO](../PlatformIO) et de [STM32CubeIDE](../STM32CubeIDE).
