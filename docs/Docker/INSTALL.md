# Docker Installation Guide


## Table of contents

1. [Introduction](#introduction)
2. [On Linux](#installLinux)
	1. [Configuration](#LinuxStep1)
	2. [Installation](#LinuxStep2)
3. [On Windows](#installWindows)
	1. [Test and Use](#test)
	2. [You encounter an error ?](#erreur)

## Introduction <a name = introduction></a>

This guide will explain how to install Docker Engine on [Linux](#installLinux) and [Windows](#installWindows). 

For a specific installation, look at the official Docker website [here](https://docs.docker.com/get-docker/).


## Installing Docker on Linux <a name = installLinux></a>

For information, this installation is done on a ___Ubuntu 22.04.1 LTS___ distribution.

### Step 1 : Docker repository configuration <a name = LinuxStep1></a>

Start by opening a command prompt and updating your **apt** with the command :

```
sudo apt-get update
```

Then, install the packages: ***ca-certificates***, ***curl***, ***gnupg*** and ***lsb-release*** which will allow you to use a repository via HTTPS. 

To do this, issue the command :

```
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

```

Now please add the official Docker GPG key, in order to configure the repository and install Docker.

First, create a directory or retrieve the key with :

```
sudo mkdir -p /etc/apt/keyrings
```

Then retrieve the key using :

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

To finish the configuration, do the following command: 

```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

### Step 2 : Install Docker Engine <a name = LinuxStep2></a>


Install ***Docker Engine***, ***containerd*** and ***Docker Compose***, using the command :

```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

You have finally finished your installation!

To test this, you can do the following command:

```
sudo docker run hello-world
```

This command will download an image and run it in the container. If there is no problem, then you should get this:

![](./.pictures/DockerHelloWorld.png)



## Installing Docker on Windows <a name = installWindows></a>

To get started, go to [this link](https://docs.docker.com/desktop/install/windows-install/).

Then install docker for windows by clicking on the **Docker Desktop for Windows** button.

Once you have downloaded the **.exe**, run it and follow the installation steps.


Launch Docker Desktop which should have been created on your desktop.

Please follow our advice [here](#error), if you encounter an error before arriving on this screen:

![](../.pictures/DockerWindowsInstalled.png)


If you get to this window, you can start by clicking on **Sign In** and create an account. Come back to this window and do or skip the tutorial.

### Test and Use :<a name = test></a>

Now that your installation is complete, you can check that docker is working by doing the commands :

```
docker run hello-world
``` 

```
docker images 
```

```
docker run -it hello-world
```

Like this: 

![](../.pictures/DockerWindowsHelloWorld.png)
![](../.pictures/DockerWindowsRunHello.png)

The **docker run** command allows in this case to build a Docker image.
The **docker images** command allows to display all your built images. And the 2nd docker run command allows to launch the image with hello-world as directory name.

If the message **"Hello From Docker !"** is displayed, then your installation is working.

Once you have built an image on a terminal with the **docker build** command, you can find them in the **Docker Desktop** application. In the images section, you will find all your images. In this section you can launch images by clicking on start. In the section **Container**, you will see your different containers as well as which image it is running. When the image and the container are launched, you will have access to the logs and to a terminal in order to use them.

![](../.pictures/DockerImageHello.png)
![](../.pictures/DockerLogsHello.png)

Now that your installation is functional, you can follow our user guides to create a functional environment for **stm32CubeIDE**, in [french](./docker_stm32cubeIDE/UserGuide. fr.md) and in [english](./docker_stm32cubeIDE/UserGuide.md) as well as for **RiotOS** in [french](./docker_riotOS/UserGuide.fr.md) and in [english](./docker_riotOS/UserGuide.md).

### You encounter an error ?<a name = erreur></a>

If you have this error:

![](../.pictures/ErreurKernel.png)

You will need to download a linux kernel.

To do so, click on the **WSL2 Linux kernel upgrade package for x64 machine**, available at step 4 of [this link](https://learn.microsoft.com/fr-fr/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package).

![](../.pictures/DownloadWSL2Kernel.png)

Once your kernel is downloaded, open a terminal and do :

```
wsl --install -d Ubuntu
```

You can find more information about this installation on [this link](https://learn.microsoft.com/en-us/windows/wsl/install). 

Now please uninstall the Docker Desktop and start the installation again from [start](#installWindows).

Have a nice installation!
