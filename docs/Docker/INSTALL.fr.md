# Guide d'installation de Docker

## Table des matières

1. [Introduction](#introduction)
2. [Sous Linux](#installLinux)
	1. [Configuration](#LinuxStep1)
	2. [Installation](#LinuxStep2)
3. [Sous Windows](#installWindows)
	1. [Test et Utilisation](#test)
	2. [Vous rencontrez une erreur ?](#erreur)

## Introduction <a name = introduction></a>

Ce guide va vous expliquer comment installer Docker Engine sous [Linux](#installLinux) et sous [Windows](#installWindows). 
Pour une installation spécifique, regardez sur le site officiel de Docker [ici](https://docs.docker.com/get-docker/).


## Installation de Docker sous Linux <a name = installLinux></a>

Pour information, cette installation est effectué sur une distribution ___Ubuntu 22.04.1 LTS___.

### Etape 1 : Configuration du dépôt Docker <a name = LinuxStep1></a>

Commencez par ouvrir une invite de commande et mettez à jour vos **apt** à l'aide de la commande :

```
sudo apt-get update
```

Ensuite, installez les packages : ***ca-certificates***, ***curl***, ***gnupg*** et ***lsb-release*** qui vous permettront d'utiliser un dépôt via HTTPS. 

Pour cela, faites la commande :

```
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

```

Maintenant, veuillez ajouter la clé GPG officiel de Docker, afin de configurer le dépôt et d'installer Docker.

Tout d'abord, créez un répertoire ou récupérer la clé avec :

```
sudo mkdir -p /etc/apt/keyrings
```

Après récupérez la clé en utilisant :

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

Pour finir la configuration, faites la commandes suivante : 

```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

### Etape 2 : Installer Docker Engine <a name = LinuxStep2></a>


Installez ***Docker Engine***, ***containerd*** et ***Docker Compose***, à l'aide de la commande :

```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

Vous avez enfin fini votre installation !

Pour tester celle-ci, vous pouvez faire la commande suivante :

```
sudo docker run hello-world
```

Cette commande va télécharger une image l'exécuter dans le conteneur. S'il n'y a pas de problème, alors vous deveriez obtenir ceci:

![](../.pictures/DockerHelloWorld.png)


## Installation de Docker sous Windows <a name = installWindows></a>

Pour commencer, allez sur [ce lien](https://docs.docker.com/desktop/install/windows-install/)

Puis installez docker pour windows en cliquant sur le bouton **Docker Desktop for Windows**.

Une fois le **.exe** téléchargé, exécutez le et suivez les étapes d'installation.


Lancez Docker Desktop qui à du se créer sur votre bureau.

Veuillez suivre nos conseils [ici](#erreur), si vous rencontrez une erreur avant d'arriver sur cet écran :

![](../.pictures/DockerWindowsInstalled.png)


Si vous arrivez bien sur cette fenêtre, vous pouvez commencer par cliquer sur **Sign In** et créez un compte. Revenez sur cette fenêtre, faites ou passez le tutoriels.

### Test et utilisation :<a name = test></a>

Maintenant que votre installation est terminée, vous pouvez vérifier que docker est fonctionnel en faisant les commandes :

```
docker run hello-world
``` 

```
docker images 
```

```
docker run -it hello-world
```

Comme ceci : 

![](../.pictures/DockerWindowsHelloWorld.png)
![](../.pictures/DockerWindowsRunHello.png)

La commande **docker run** permet dans ce cas de construire une image Docker.
La commande **docker images** permet d'affficher toutes vos images construitent. Et la 2ème commande docker run permet de lancer l'mage qui à hello-world comme nom de répertoire.

Si le message **"Hello From Docker !"** s'affiche, alors c'est que votre installation fonctionne.

Une fois que vous avez construit une image sur un terminal avec la commande **docker build**, vous pourrez les retrouver dans l'application **Docker Desktop**. Dans la parties images, vous retrouverez toutes vos images. Dans cette section vous pourrez lancer des images en cliquant sur start. Dans la section **Conténer**, vous verrer vos différents conténer ainsi que quel image il exécute. Quand l'image et le conténer seront lancer, vous aurez accès aux logs ainsi qu'à un terminal afin d'utiliser ceux-ci.

![](../.pictures/DockerImageHello.png)
![](../.pictures/DockerLogsHello.png)

Maintenant que votre installation est fonctionnelle, vous pouvez suivre nos guide d'utilisation pour créer un environnement fonctionnel pour **stm32CubeIDE**, en [français](./docker_stm32cubeIDE/UserGuide.fr.md) et en [anglais](./docker_stm32cubeIDE/UserGuide.md) ainsi que pour **RiotOS** en [français](./docker_riotOS/UserGuide.fr.md) et en [anglais](./docker_riotOS/UserGuide.md).

### Vous rencontrez une erreur lors de l'installation ?<a name = erreur></a>

Si vous avez cette erreur :

![](../.pictures/ErreurKernel.png)

Il va falloir que vous téléchargiez un noyau linux.

Pour cela cliquez sur le **package de mise à jour du noyau Linux WSL2 pour machine x64**, disponible à l'étape 4 de [ce lien](https://learn.microsoft.com/fr-fr/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package).

![](../.pictures/DownloadWSL2Kernel.png)

Une fois votre kernel téléchargé, ouvrez un terminal et faites :

```
wsl --install -d Ubuntu
```

Vous pouvez trouver plus d'information concernant cette installation sur [ce lien](https://learn.microsoft.com/en-us/windows/wsl/install). 

Maintenant veuillez désinstaller le Docker Desktop et recommencez l'installation depuis le [début](#installWindows).

Bonne fin d'installation !
