# Guide d’utilisation du docker STM32cubeIDE

1. [Introduction](#intro)
2. [Installation](#install)
3. [Utilisation du conteneur STM32cubeIDE](#use)
	1. [Démarrer le conteneur](#start)
	2. [Utiliser STM32cubeIDE en mode headless](#open)
		+ [Compiler un projet](#compile)
		+ [Autres commandes utiles](#other)
	
	
	
## Introduction <a name = intro></a>

Ce guide vous expliquera la marche à suivre pour utiliser STM32cubeIDE dans un conteneur Docker. Nous détaillerons ici l'utilisation de STM32cubeIDE en ligne de commande.

## Installation <a name = install></a>
Dans un terminal, exécutez la commande:

	docker build -t stm32[:votre_tag] /path/to/project/Dockerfile

Cette commande vous permet construire une image docker à partir du Dockerfile et de la renommer en **stm32**. **/path/to/project/Dockerfile** doit correspondre à l'emplacement du Dockerfile. 
Si vous souhaitez changer le nom de votre image docker, vous pouvez le faire directement dans la commande en remplaçant le nom **stm32** par un autre nom. Veillez cependant à n'utiliser que des lettres **minuscules** et des chiffre. Vous pouvez aussi renseigner un _tag_ en ajoutant **:votre_tag** collé au nom de votre image (le tag par défaut lorsque vous n'en renseignez aucun est ___lastest___).
Vous pouvez aussi renommer votre conteneur après avoir exécuté la commande précédente. Pour cela, utilisez la commande :

	docker tag <ancien_nom>[:<tag>] <nouveau_nom>[:<tag>]

Cette commande permet de renommer n'importe que image docker. Les paramètres entre crochet ( _[:tag]_ ) sont **optionnels**. 

## Utilisation du conteneur STM32cubeIDE <a name = use></a>
### Démarrer et arrêter le conteneur <a name = start></a>

Pour utiliser l'image docker avec STM32cubeIDE, exécuter la commande suivante si vous n'avez pas renommé votre image docker :

	docker run -it -v -v /outside/conteneur/path:/inside/conteneur/path <nom_docker>

+ L'option **-it** permet de rendre le conteneur interactif.
+ L'option **-v** permet de lier un répertoire de votre machine à un des répertoire de votre conteneur pour que les fichier qui sont a l'interieur soient disponible depuyis votre machine une fois que vous aurez quitté le conteneur. Voici un exemple de commande pour lancer votre conteneur docker:

	docker run -it -v /home/samuel/Documents/Projet:/ProjetSTM32 stm32

Une fois que vous avez lancé votre image docker, vous aurez accès a un terminal pouvant utiliser les commandes d'une distribution Ubuntu. 
Pour arrêter le conteneur, entrez la commande:

	exit

### Utiliser STM32cubeIDE en mode headless

Pour utiliser STM32cubeide en ligne de commande, vous aurez besoin d'avoir un projet existant dans votre docker. Vous pouvez lier votre workspace contnant votre projet à votre docker avec la commande vu [au dessus](#start). Par défaut, lorsque vous lancez le conteneur Docker, vous pouvez y accéder avec la commande:

	 cd /stm32cubeide/stm32cubeide/
	 

#### Compiler un projet <a name = compile></a>

Pour compiler un projet STM32cubeIDE, vous devez utiliser la commande suivante:

	<path/to/stm32cubeide> --launcher.suppressErrors -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild -data </path/to/workspace> -import <path/to/project> -cleanBuild <name_project>


* Le paramètre **-data** permet de spécifier le répertoire contenant votre projet. 
* Le paramètre **-import** permet d'importer le projet spécifié
* Le paramètre **-cleanBuild** permet de spécifier quel(s) projet(s) compiler.


Voici un exemple de commande sachant que le chemin vers stm32cubeide dans le docker est **stm32cubeide/stm32cubeide/stm32cubeide** et que le projet se trouve dans le workspace est **/ProjetSTM32** et que le chemin du projet est **/ProjetSTM32/OTEM** :

	stm32cubeide/stm32cubeide/stm32cubeide --launcher.suppressErrors -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild -data /ProjetSTM32 -import /ProjetSTM32/OTEM -cleanBuild OTEM


#### Autres paramètres utiles <a name = other></a>

- -import     {[uri:/]/path/to/project}
- -importAll  {[uri:/]/path/to/projectTreeURI} Import all projects under URI
- -build      {project_name_reg_ex{/config_reg_ex} | all}
- -cleanBuild {project_name_reg_ex{/config_reg_ex} | all}
- -no-indexer Disable indexer
- -markerType Marker types to fail build on {all | cdt | marker_id}
- -printErrorMarkers Print all error markers
- -I          {include_path} additional include_path to add to tools
- -include    {include_file} additional include_file to pass to tools
- -D          {prepoc_define} addition preprocessor defines to pass to the tools
- -E          {var=value} replace/add value to environment variable when running all tools
- -Ea         {var=value} append value to environment variable when running all tools
- -Ep         {var=value} prepend value to environment variable when running all tools
- -Er         {var} remove/unset the given environment variable
- -T          {toolid} {optionid=value} replace a tool option value in each configuration built
- -Ta         {toolid} {optionid=value} append to a tool option value in each configuration built
- -Tp         {toolid} {optionid=value} prepend to a tool option value in each configuration built
- -Tr         {toolid} {optionid=value} remove a tool option value in each configuration built

Les paramètres des options sont traduit en chaîne de caractère, liste de chaînes séparées par des virgules ou valeur booléenne en fonction du type de paramètre attendu.