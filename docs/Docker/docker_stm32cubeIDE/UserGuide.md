# STM32cubeIDE docker user guide

1. [Introduction](#intro)
2. [Setup](#install)
3. [How to use the STM32cubeIDE container](#use)
	1. [Start container](#start)
	2. [Use STM32cubeIDE in headless mode](#headless)
		+ [Compile a project](#compile)
		+ [Other useful Commands](#other)
	
	
	
## Introduction <a name = intro></a>

This guide will walk you through how to use STM32cubeIDE in a Docker container. We will detail here the use of STM32cubeIDE in command line.

## Setup <a name = install></a>
In a terminal, run the command:

	docker build -t <container_name> /path/to/project/Dockerfile

This command allows you to download a docker image containing STM32cubeIDE and renames this image to **stm32**. If you want to change the name of your docker image, you can do it directly in the command by replacing the name **stm32** with another name. However, be sure to use only **lowercase** letters and numbers. You can also fill in a _tag_ by adding **:your_tag** pasted to the name of your image (the default tag when you don't fill in any is ___latest___).
You can also rename your container after running the previous command. To do this, use the command:

	docker tag <old_name>[:tag] <new_nom>[:tag]

This command allows you to rename any docker image. Parameters in square brackets ( _[:tag]_ ) are **optional**.

## How to use the STM32cubeIDE container <a name = use></a>
### Start container <a name = start></a>

To use the docker image with STM32cubeIDE, run the following command if you haven't renamed your docker image:

	docker run -it stm32

Otherwise, replace **stm32** with your container name. You can also specify a tag if needed.

Once you've launched your docker image, you'll have access to a terminal that can use commands from an Ubuntu distribution.
To stop the container, enter the command:

	exit

### Use STM32cubeIDE in headless mode <a name = headless></a>

To use STM32cubeide on the command line, go to the directory containing STM32cubeIDE. By default, when you launch the Docker container, you can access it with the command:

	 cd /stm32cubeide/stm32cubeide/
	 

#### Compile a project <a name = compile></a>

To compile an STM32cubeIDE project, you can run the command:

	/stm32cubeide/stm32cubeide/stm32cubeide --launcher.suppressErrors -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild -data "/path/to/project" -build *
	
* The **-data** parameter allows you to specify the directory containing your project. 
* The **-build** parameter allows to specify which file(s) to compile.

#### Other useful Commands <a name = other></a>

- -import     {[uri:/]/path/to/project}
- -importAll  {[uri:/]/path/to/projectTreeURI} Import all projects under URI
- -build      {project_name_reg_ex{/config_reg_ex} | all}
- -cleanBuild {project_name_reg_ex{/config_reg_ex} | all}
- -no-indexer Disable indexer
- -markerType Marker types to fail build on {all | cdt | marker_id}
- -printErrorMarkers Print all error markers
- -I          {include_path} additional include_path to add to tools
- -include    {include_file} additional include_file to pass to tools
- -D          {prepoc_define} addition preprocessor defines to pass to the tools
- -E          {var=value} replace/add value to environment variable when running all tools
- -Ea         {var=value} append value to environment variable when running all tools
- -Ep         {var=value} prepend value to environment variable when running all tools
- -Er         {var} remove/unset the given environment variable
- -T          {toolid} {optionid=value} replace a tool option value in each configuration built
- -Ta         {toolid} {optionid=value} append to a tool option value in each configuration built
- -Tp         {toolid} {optionid=value} prepend to a tool option value in each configuration built
- -Tr         {toolid} {optionid=value} remove a tool option value in each configuration built
 
Tool option values are parsed as a string, comma separated list of strings or a boolean based on the option's type