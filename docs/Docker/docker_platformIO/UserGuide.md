# Dockerfile Usage Guide for PlatformIO

## Table of Contents

1. [Introduction](#introduction)
2. [Running Dockerfile](#RunningDockerfile)

## Introduction <a name = introduction></a>

In this guide, you will learn how to build a Docker image from a Dockerfile to launch a container with your **PlatformIO** project, which you can use without the need to install this IDE on your machine. Once the container is launched with the commands provided in this document, your working environment will be ready to use within the container.

## Running Dockerfile <a name = RunningDockerfile></a>

First, download the [Dockerfile](./Dockerfile) from this GitLab repository. Then, in a terminal, run the following command:

```
docker build -t platformio /path/to/Dockerfile
```


The path **/path/to/file** should point to the location of the Dockerfile you just downloaded. If you are in the folder containing the **Dockerfile**, you can use a "." instead of the path.

This command will build a Docker image with the configured **PlatformIO** environment and useful tools for this environment.

The **-t** option allows you to name your Docker image. You can also add a **tag**. By default, this tag is set to 'latest', but you can enter a name that will save your storage space because if you already have an image with this tag, it will be overwritten by the one built.

If you want to name your image and add a tag, you can run the following command:

```
sudo docker build -t <name>:tag .
```


Replace `<name>` with your chosen name (without the <...>), 'tag' with the tag you want to give to your image, and '.' with the path to your Dockerfile (if it is not in your current directory).

Next, to verify that your image has been built correctly and to get its ID, you can run the following command:

```
sudo docker images
```


This command will display your different images with their repository name, tag, ID, time since creation, and size:

![](./images/docker_images.png)

In this case, this Dockerfile will build a **debian** and a **platformio** image.

Now, the next step is to launch your container to use PlatformIO.

To use **PlatformIO** with an existing project and to upload code to the microcontroller, the container must have access to:

- Your *PlatformIO* project
- The port to which the microcontroller is connected

To do this, you should not just run the container from the image with the command:

```
sudo docker run -it platformio
```


Start by downloading the **PlatformIO** project from the GitLab repository (the folder containing the *project_name.ini* file). Once downloaded, and saved where you want, remember the absolute path to it.

If you don't know the path, check your file manager, or run the `pwd` command in a terminal (at the project location).

Now you are ready to launch your container correctly to take advantage of the **PlatformIO** environment.

In the command you will run, you will link your PlatformIO project directory with a directory inside the container (e.g., */user/dev*). And to upload code to the microcontroller, the container must have the privilege to access the computer's ports (i.e., the */dev* directory), so this directory will also be linked.

Here is the command to run:

```
sudo docker run -it --privileged -v /dev:/dev -v /chemin/absolue/vers/le/projet:/user/dev platformio
```

In this command, *-it* allows you to launch the container interactively, *-v* allows you to mount and link directories (in the form: */outside/container/path:/inside/container/path*), *--privileged* grants additional privileges to the container (in your case, access to */dev* and thus the ports). Finally, "platformio" at the end of the command refers to the name of the image you built earlier.

![](./images/run.png)

We can see that from the container we have direct access to the *pio* commands, that we have access to the linked project and access to the ports.

Once you are done and want to exit the container, run the following command:

```
exit
```

Of course, when you exit the container, the linked directory will keep the changes made during development inside the container.

Now you only have to use **PlatformIO**. You can check the tutorials available in [French](../../PlatformIO/TUTORIAL.fr.md) and [English](../../PlatformIO/TUTORIAL.md).