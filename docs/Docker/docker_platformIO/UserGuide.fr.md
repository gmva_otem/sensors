# Guide d'utilisation du Dockerfile pour PlatformIO

## Table des matières

1. [Introduction](#introduction)
2. [Lancer Dockerfile](#RunDockerfile)

## Introduction <a name = installLinux></a>

Dans ce guide, vous verrez comment construire une image Docker depuis un Dockerfile, afin de lancer un conteneur avec votre projet **PlatformIO** que vous pourrez utiliser sans avoir besoin de faire l'installation de cet IDE sur votre machine. Une fois le conteneur lancé avec les commande qui vous seront indiquées dans ce documents, votre environnement de travail sera directement prêt dans le conteneur.


## Lancer DockerFile <a name = RunDockerfile></a>
Premièrement, téléchargez le fichier [Dockerfile](./Dockerfile) depuis ce Gitlab. Puis, dans un terminal, exécutez la commande:

```
docker build -t platformio /path/to/Dockerfile
```

Le chemin **/path/to/file** doit indiquer l'emplacement du Dockerfile que vous venez de télécharger. Si vous vous trouvez dans le dossier ou se situe le **Dockerfile**, vous pouvez mettre un "." à la place du path.

Cette commande va construire une image docker avec l'environnement **PlatformIO** configuré, ainsi qu'avec des outils utiles à cet environnement.

L'option **-t** permet de nommer votre image Docker. Vous pouvez également ajouter un **tag**. Par défaut, ce tag est à latest, mais vous pouvez saisir un nom qui vous permettra d'économiser votre stockage car si vous avez déjà une image avec ce tag alors elle sera écrasé par celle construite.


Si vous  souhaitez nommer votre image ainsi que lui ajouter un tag, vous pouvez exécuter la commande:

```
sudo docker build -t <name>:tag .
```

Pensez a remplacer \<name> par votre nom (sans les <...>), tag par le tag que vous voulez donner à votre image, et le "." par le chemin vers votre Dockerfile (si il n'est pas dans votre répertoire courant).

Ensuite, pour vérifier que votre image s'est bien construites et pour connaître l'ID de celle-ci, vous pouvez exécuter la commande :

```
sudo docker images
```

Cette commande va afficher vos différentes images avec leur nom de dépôt, tag, ID, temps depuis la création et taille :

![](./images/docker_images.png)

En l'occurrence, ce Dockerfile, va vous construire une image **debian** et une image **platformio**. 

Maintenant, la prochaine étape est de lancer votre conteneur pour utiliser platformio. 

Or pour pouvoir utiliser **PlatformIO** avec le projet existant et de pouvoir téléversé du code vers le microcontrôleur, il faut que le conteneur ai accès :
- A votre projet *PlatformIO*
- Au port auquel est branché le microcontrôleur

Pour cela, il ne faut pas juste lancer le conteneur à partir de l'image avec la commande :

```
sudo docker run -it platformio
```

Vous allez commencer par télécharger depuis le GitLab, le projet **PlatformIO** (le dossier ou se trouve le *nom_du_projet.ini*). Une fois téléchargé, et enregistrer ou vous le souhaité, retenez le chemin absolue de celui-ci.

Si vous ne le connaissez pas, regarder dans votre gestionnaire de fichier, ou faite la commande *pwd* dans un terminal (à l'emplacement du projet).

Maintenant vous êtes prêt pour lancer votre conteneur correctement afin de pouvoir profiter de l'environnement **PlatformIO**. 

Dans la commande que vous allez exécuter, vous allez lier votre répertoire contenant le projet Pio avec un répertoire du conteneur (par exemple */user/dev*). Et pour pouvoir téléverser le code sur le microcontrôleur, il faut que le conteneur ai le privilège d'avoir accès aux port du pc (soit au répertoire */dev*), on va donc lié également ce répertoire. 

Voici donc la commande à effectuer :

```
sudo docker run -it --privileged -v /dev:/dev -v /chemin/absolue/vers/le/projet:/user/dev platformio
```

Dans cette commande, le *-it* permet de lancer le conteneur de manière interactif, le *-v*, permet de monter des répertoire donc de les lier (sous la form : */outside/conteneur/path:/inside/conteneur/path*), le *--privileged*, permet d'accoder des privilèges supplémentaire au conteneur  dans votre cas d'avoir accès au */dev* donc aux ports. Et pour finir le "platformio" à la fin de la commande correspond au nom de l'image que vous avez construit précédemment.

![](./images/run.png)

On peux voir que depuis le conteneur on à directement accès aux commande *pio*, que l'on à bien accès au projet lié et accès aux ports.

Une fois que vous avez terminé et que vous voulez quitter ce conteneur, exécutez la commande :

```
exit
```

Bien sûr, lorsque vous quitter votre conteneur, le répertoire lié à celui-ci garde les modification apporter lors du développement à l'intérieur du conteneur.

Il ne vous reste qu'à utiliser **PlatformIO**. Vous pouvez aller voir les tutoriels mis à votre disposition en [français](../../PlatformIO/TUTORIAL.fr.md) et en [anglais](../../PlatformIO/TUTORIAL.md).