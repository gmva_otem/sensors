# Notice d'utilisation des outils de l'environnement de développement STM32CubeIDE

Dans ce tutoriel, nous allons vous présenter l'environnement de développement STM32CubeIDE ainsi que l'utilisation de ses outils.

## Sommaire

1. [Création d'un projet](#newproject)
2. [Compile file](#compilefile)
3. [Execute file](#executefile)

## Création d'un projet <a name = newproject>

Nous allons commencer sur cette page :
![](./images/mainPage.png)

Cliquez sur "Start new STM32 project", sauf si vous voulez :
    -Démarrer un projet a partir d'un fichier STM32CubeMX
    -Importer un projet STM32CubeIDE
    -Démarrer un projet Exemple

