# Guide d'installation de l'environnement de développement STM32cubeIDE

## Table des matières

1. [Introduction](#introduction)
2. [Installation de STM32CubeIDE sur Windows](#Windows)
	1. [Téléchargement de l'environnement](#CubeDownload)
	2. [Configuration de STM32CubeIDE](#CubeConfig)
3. [Installation de STM32CubeIDE sur Linux](#Linux)
	1. [Télécharger l'environnement STM32CubeIDE](#LinuxCubeDownload)
	2. [Utiliser l'invite de commande pour terminer l'installation](#LinuxCubeInstall)

## Introduction 

Ce fichier explique comment installer l'environnement de programmation STM32CubeIDE. L'installation peut être faite sur Windows et Linux.

## Installation de STM32CubeIDE sur Windows<a name = Windows></a>
Cette première partie vous accompagnera pour l'installation de STM32CubeIDE sur Windows.

### Étape 1: Télécharger l'environnement STM32CubeIDE <a name = CubeDownload></a>
Tout d'abord, il faut télécharger l'exécutable sur [le site officiel de STM32cubeIDE.](https://www.st.com/en/development-tools/stm32cubeide.html)

Cliquez sur le bouton "Get Software", cela devrait descendre votre fenêtre vers un tableau avec une liste d'installation. Choisissez **STM32CubeIDE Windows Installer**.

![](./images/Tableau.png) 

Cliquez sur "Get Latest". Acceptez la licence d'utilisation puis renseignez vos info personnelles. Vous devez renseignez un adresse e-mail à laquelle vous avez accès car c'est sur cette adresse e-mail que vous recevrez un lien pour télécharger l’exécutable.
Peu de temps après, vous recevrez un e-mail de confirmation de "STMicroelectronics". Vous pouvez cliquer sur "Download now", cela devrait ouvrir une page et lancer le téléchargement.

![](./images/Zip.png) 

Une fois le téléchargement du .zip complété, vous devez extraire le contenu du zip, puis exécuter l'installateur de STM32cubeIDE.

![](./images/Appli.png) 

### Étape 2: Configurer l'environnement STM32CubeIDE <a name = CubeConfig></a>

Au lancement de l'installateur, vous pourrez choisir des composant à installer en plus. Par défaut, tout est coché et nous vous conseillons de les installer sauf si vous avez des besoins spécifiques. 
Vous pouvez ensuite appuyer sur **Install** pour débuter l'installation. A la fin de l'installation, cliquez sur **Next** puis **Finish**.

![](./images/Components.png) 

Pour débuter un nouveau projet STM32cubeIDE, ouvrez l'application. Sur la page qui vient de s'ouvrir, vous devez choisir le chemin vers le répertoire qui contiendra votre projet. C'est dans celui ci que vous trouverez tous les fichiers générés par STM32cubeIDE. Si le pare-feu Windows ouvre une fenêtre, autorisez l'application.

![](./images/Desktop.png) 

Maintenant, vous pouvez commencer à développer votre projet STM32cubeIDE sur Windows!

## Installation de STM32CubeIDE sur Linux<a name = Linux></a>
Cette seconde partie vous accompagnera pour l'installation de STM32CubeIDE sur Linux.

### Étape 1: Télécharger l'environnement STM32CubeIDE <a name = LinuxCubeDownload></a>
Tout d'abord, il faut télécharger l'exécutable sur [le site officiel de STM32cubeIDE.](https://www.st.com/en/development-tools/stm32cubeide.html)

Cliquez sur le bouton "Get Software", cela devrait descendre votre fenêtre vers un tableau avec une liste d'installation. Choisissez **STM32CubeIDE Generic Linux Installer**.

![](./images/Tableau.png) 

Cliquez sur "Get Latest". Acceptez la licence d'utilisation puis renseignez vos info personnelles. Vous devez renseignez un adresse e-mail à laquelle vous avez accès car c'est sur cette adresse e-mail que vous recevrez un lien pour télécharger l’exécutable.
Peu de temps après, vous recevrez un e-mail de confirmation de "STMicroelectronics". Vous pouvez cliquer sur "Download now", cela devrait ouvrir une page et lancer le téléchargement.

![](./images/zipLinux.png)

Une fois le téléchargement du .zip complété, déplacez vous dans le répertoire dans lequel vous avez téléchargé le fichier zip.
Pour dézipper l'installateur, exécutez la commande:
	
	unzip en.st-stm32cubeide_1.n°version_amd64.sh.zip 


Pour choisir la destination du fichier dézippé, utilisez l'option **-d**. Exemple:

	unzip en.st-stm32cubeide_1.11.0_13638_20221122_1308_amd64.sh.zip -d /tmp/stm32

Si vous n'avez pas installé le paquet **unzip**, vous pouvez le faire en exécutant la commande:

	apt-get install -y unzip

### Étape 2: Utiliser l'invite de commande pour effectuer l'installation <a name = LinuxCubeInstall></a>

Ouvrez un terminal et déplacez vous dans le répertoire contenant le fichier d'installation. Le nom de ce fichier ressemblera à __en.st.stm32cubeide_1.**n°version**_amd64.sh__.

Puis, exécutez la commande :

	sh ./st-stm32cubeide_VERSION_amd64.sh

La version sera écrite dans le nom du fichier d'installation que vous aurez extrait. Dans notre exemple, la version dans l'image ci-dessus, la version est 1.11.0_13638_20221122_1308, ce qui nous donne la commande :

	sh ./st-stm32cubeide_1.11.0_13638_20221122_1308_amd64.sh

Cela lancera l'installation sur votre terminal.

Suivez les étapes d'installation de sur votre terminale:
1 - Pour accepter la licence, entrez **y**.
2 - Sélectionnez un emplacement d'installation pour votre application puis appuyez sur **Entrer**. Le répertoire par défaut est /opt/st/ (écrit entre crochet)
3 - Vous aurez le choix pour installer le plugin Segger J-Link. Nous vous conseillons de l'installer sauf si vous avez des besoins spécifiques.
4 - Déroulez le contrat d'utilisation de Segger J-Link et acceptez-le en entrant **y**.

ATTENTION, LE CONTRAT EST LONG. Vous pouvez facilement rater la ligne de confirmation en passant rapidement. Si cela arrive, vous devrez recommencer toute la procédure d'installation en re-tapant la commande sh et potentiellement choisissant un différent dossier d'installation, voici une image qui peut vous servir de repère pour ne pas passer la ligne de confirmation :

![](./images/Contrat.png)

Pour lancer STM32cubeIDE, vous pouvez le lancer sois depuis le terminale, soit depuis votre explorateur de fichier.

Pour le lancer depuis le terminale, vous devez vous rendre dans le répertoire d'installation et exécuter le fichier **stm32cubeide**:

	cd /opt/st/st-stm32cubeide_1.11.0/
	./stm32cubeide

Pour le lancer depuis votre explorateur de fichier, vous devez vous rendre dans votre répertoire d'installation et double-cliquer sur le fichier **stm32cubeide**.

![](./images/installFolder.png)

Une fois que vous avez ouvert l'application vous devez choisir le chemin vers le répertoire qui contiendra votre projet sur la page qui vient de s'ouvrir. C'est dans celui ci que vous trouverez tous les fichiers générés par STM32cubeIDE.

Maintenant, vous pouvez commencer à développer votre projet STM32cubeIDE sur Linux!