# Guide d'utilisation du Dockerfile pour RiotOS

## Table des matières

1. [Introduction](#introduction)
2. [Lancer Dockerfile](#RunDockerfile)

## Introduction <a name = installLinux></a>

Dans ce guide, vous verrez comment construire un centeneur Docker avec les outils RiotOS depuis un Dockerfile.


## Lancer DockerFile <a name = RunDockerfile></a>
Premièrement, téléchargez le fichier [Dockerfile](./Dockerfile) depuis ce Gitlab. Puis, exécutez la commande:

	docker build -t riot /path/to/Dockerfile
	

Le chemin **/path/to/file** doit indiquer l'emplacement du Dockerfile que vous venez de télécharger.

Cette commande va construire une image docker avec l'environnement RiotOS configuré, ainsi qu'avec des outils utiles à cet environnement.

L'option **-t** permet de nommer votre image Docker. Vous pouvez également ajouter un **tag**. Par défaut, ce tag est à latest, mais vous pouvez saisir un nom qui vous permettra d'économiser votre stockage car si vous avez déjà une image avec ce tag alors elle sera écrasé par celle construite.


Si vous  souhaitez nommer votre image ainsi que lui ajouter un tag, vous pouvez exécuter la commande:

	docker build -t <name>:tag .


Pensez a remplacer \<name> par votre nom (sans les <...>)

Ensuite, pour connaître l'ID de l'image créée, vous pouvez exécuter la commande :

	docker images

Cette commande va afficher vos différentes images avec leur nom de dépôt, tag, ID, temps depuis la création et taille :

![](../../.pictures/ExempleDockerImagesRiot.png)

Pour faire exécuter votre conteneur, vous pouvez la lancer en utilisant son nom :


	docker run -it -v /outside/conteneur/path:/inside/conteneur/path <nom_docker>


+ L'option **-it** permet de rendre le conteneur interactif.
+ L'option **-v** permet de lier un repertoire de votre machine a un des repertoire de votre conteneur pour que les fichier qui sont a l'interieur soient disponible depuyis votre machine une fois que vous aurez quitté le conteneur. Voici un exemple de commande pour lancer votre conteneur docker:

	docker run -it -v ~/Documents/Projet:/user/dev <nom_docker>


Vous allez alors vous trouver dans le répertoir /user/dev de votre nouvelle machine virtuelle. Toutes les fonctionnalités liées à RiotOS sont utilisables, vous pouvez donc commencer à programmer.

Une fois que vous avez terminé et que vous voulez quitter ce conteneur, exécutez la commande :

	exit

Il ne vous reste qu'à utiliser RiotOS. Vous pouvez aller voir les tutoriels de Riot mis à votre disposition en [français](../../riot-os/TUTORIAL.fr.md) et en [anglais](../../riot-os/TUTORIAL.md).