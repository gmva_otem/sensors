# Dockerfile for RiotOS user guide

## Table of contents

1. [Introduction](#introduction)
2. [Run Dockerfile](#RunDockerfile)

## Introduction <a name = introduction></a>

This guide will explain how to launch a Dockerfile.


## Run DockerFile<a name = RunDockerfile></a>

First, download the [Dockerfile](./Dockerfile) file from this Gitlab. Then run the command:

	docker build -t riot /path/to/Dockerfile

The path **/path/to/file** should point to the location of the Dockerfile you just downloaded.

This command will build a docker image with the RiotOS environment configured, as well as tools useful to that environment.

The **-t** option allows you to name your Docker image. You can also add a tag. By default, this **tag** is at latest, but you can enter a name that will save your storage because if you already have an image with this tag then it will be overwritten by the one built.

If you want to name your image, you can run the command :


	docker build -t <name>:tag .

Remember to replace \<name> with your name (without the <...>)

Then, to know the ID of the created image, you can execute the command :

	docker images 

This command will display your different images with their repository name, tag, ID, time since creation and size:

![](../../.pictures/ExempleDockerImagesRiot.png)

To run your container, you can launch it using its name:

	docker run -it -v /outside/conteneur/path:/inside/conteneur/path <nom_docker>

+ The **-it** option makes the container interactive.
+ The **-v** option allows you to link a directory on your machine to one of the directories of your container so that the files inside are available from your machine once you have left the container. Here is an example command to launch your docker container:

	docker run -it -v ~/Documents/Projet:/user/dev <nom_docker>
	

You will then be in the /user/dev directory of your new virtual machine. All the features related to RiotOS are usable, so you can start programming.

Once you are done and you want to leave this container, do :

	exit

Now you can use the RiotOS development environment. You can check the Riot tutorials available in [french](../../riot-os/TUTORIAL.fr.md) and [english](../../riot-os/TUTORIAL.md).