# Guide d'installation de l'environnement de développement RiotOS

## Table des matières

1. [Introduction](#introduction)
2. [Installation et configuration de GIT](#installation)
	1. [Installer GIT](#GitInstall)
	2. [Configurer GIT](#GitConfig)
	3. [Clonner le dépôt RiotOS depuis GitHub](#GitClone)
3. [Installation des outils nécessaires au développement](#InstallRiotOSEnv)

## Introduction 

Ce guide explique comment installer et configurer un environnement de développement RiotOS. Nous tenons à préciser que nous effectuons notre installation sur une distribution ___Ubuntu 22.04.1 LTS___.

## Installation et configuration de GIT <a name = installation></a>

Pour pouvoir utiliser les outils et l'environnement de développement que propose RiotOS, vous avez besoin du dépôt github de RiotOS. Vous allez donc devoir le cloner dans un répertoire à l'aide de GIT. 

### Etape 1: Installer GIT <a name = GitInstall></a>

Pour cette première étape, vous allez ouvrir un terminal et entrer la commande:

```
sudo apt-get install git
```

Pour une autre distribution qu'Ubuntu, vous pouvez trouver les différentes commandes d'installation sur le site officiel de GIT disponible [ici](https://git-scm.com/download/linux).

### Etape 2: Configurer GIT <a name = GitConfig></a>

Cette étape permet de pouvoir utiliser GIT pour travailler sur des dépôts en ligne (GitHub, GitLab, etc...).

Faites les commandes suivantes :

```
git config --global user.name "your_name" 
git config --global user.email "your.email@x.y" 
```
		
Pour que la configuration fonctionne correctement, veillez à renseigner le nom et l'adresse exact que vous avez entrer sur votre compte gitHub ou gitLab.

### Etape 3: Cloner le dépot Riot-OS/RIOT<a name = GitClone></a>

Maintenant, vous allez récupérer les fichiers présents sur le dépot GitHub disponible [ici](https://github.com/RIOT-OS/RIOT). Pour cela, vous allez dans un premier temps créer un répertoire qui accueillera ces fichiers grâce à la commande:

```
git init
```

Cette commande créera un nouveau répertoire **.git** dans le répertoire courant. Si vous voulez placer votre dépot local à un endroit précis de votre arborescense, assurez-vous d'être dans ce répertoire lorsque vous effectuerez la commande.

Ensuite, vous pouvez cloner le code GIT de RiotOS avec la commande:

```
git clone https://github.com/RIOT-OS/RIOT.git
```

Après cela, vous disposez des fichiers à disposition avec RiotOS.

## Installation des outils nécessaires au développement <a name = InstallRiotOSEnv></a>

Pour pouvoir utiliser l'environnement de développement RiotOS, vous allez avoir besoin d'outils de développement spécifiques, en fonction de l'archictecture de votre projet ainsi que de votre matériel. Ainsi, vous serez ammenés à utiliser certains de ces outils :

- Outils essentiels de développement système (NU Make GCC, standard C library headers)
- **Git** (que vous avez déjà installé et configuré plus haut)
- **unzip** ou **p7zip**
- **GDB** dans la variante multiarch (alternative : installez pour chaque architecture que vous ciblez le **package GDB correspondant**)
- **wget** ou **curl**
- **python3**
- **pyserial** (paquet de distribution Linux souvent nommé **python3-serial** ou **py3-serial**)
- **Doxygen** pour la génération de la documentation

Pour votre configuration, vous allez installer les outils préconnisés par RiotOS sur leur site officiel. Si vous avez d'autres outils que ceux-ci et qu'ils sont compatibles avec les librairies RiotOS, vous pouvez tout de même les utiliser sans problème.

Pour installer ces outils, il faut entrer la commande:

```
sudo apt install git gcc-arm-none-eabi make gcc-multilib libstdc++-arm-none-eabi-newlib openocd gdb-multiarch doxygen wget unzip python3-serial
```

Maintenant, vous disposez de tous les outils pour utiliser l'environnement de développement RiotOS.

### Bon développement !
