# Guide to install the RiotOS developement environment

## Table of contents

1. [Introduction](#introduction)
2. [Installation and configuration of GIT](#installation)
	1. [Install GIT](#GitInstall)
	2. [Configure GIT](#GitConfig)
	3. [Clone the RiotOS repository from GitHub](#GitClone)
3. [Installing the tools needed for development](#InstallRiotOSEnv)

## Introduction 

This guide explain how to install and configure the RiotOS development environment. We would like to point out that we did this installation on an ___Ubuntu 22.04.1 LTS___ distribution.

## Installation and configuration of GIT <a name = installation></a>

To be able to use the tools and development environment that RiotOS offers, we need the RiotOS GitHub 
repository. So we're going to clone it into a directory using GIT.


### Step 1: Install GIT <a name = GitInstall></a>

For this first step, you will open a terminal and input the command :

```
get install git
```

If you have a different distribution than Ubuntu, you can look at the differents install commands on the official GIT website, available [here](https://git-scm.com/download/linux).

### Step 2: Configure GIT <a name = GitConfig></a>

This step allows you to use GIT to work on online repositories (GitHub, GitLab, ...).

input the following commands :

```
git config --global user.name "your_name" 
git config --global user.email "your.email@x.y"
```

For a good configuration, be sure to fill in the exact name and email address that you have entered on your GitHub or GitLab account.

### Step 3: Clone the Riot-OS/RIOT repository <a name = GitClone></a>

Now, you will retrieve the files from the GitHub repository, available [here](https://github.com/RIOT-OS/RIOT). To do this, you will need to create a **git directory** that will host these cloned files. 

First of all, input the command : 

```
git init
```

This command will create a new **.git** directory in the current directory. If you want to put your cloned repository in a specific location, make sure you are in that directory when you run the command.

Then, you can clone the RiotOS GIT code with the command :

```
git clone https://github.com/RIOT-OS/RIOT.git
```

After that, you have the files available with RiotOS.

## Installing the tools needed for development<a name = InstallRiotOSEnv></a>

To use the development environment of RiotOS, you will need specifics tools, depending on the architecture of your project and your hardware. So, you will have to use some of these tools :

- Essential system development tools (NU Make GCC, standard C library headers)
- **Git** (that you have already installed and configured above)
- **unzip** or **p7zip**
- **GDB** in the variant multiarch (alternative : install for each architecture you are targeting the **corresponding GDB package**)
- **wget** or **curl**
- **python3**
- **pyserial** (often named linux distribution pack **python3-serial** or **py3-serial**)
- **Doxygen** for the generation and the documentation

For your configuration, you will need to install the tools recommanded by RiotOS on their official website. But if you have other tools than these and if they are compatible with RiotOS libraries, you can still use them without any problems.

To install these tools, you have to use the command :

```
sudo apt install git gcc-arm-none-eabi make gcc-multilib libstdc++-arm-none-eabi-newlib openocd gdb-multiarch doxygen wget unzip python3-serial
```

Now, you have all the tools you need to use RiotOS' development environment.

### Good development !