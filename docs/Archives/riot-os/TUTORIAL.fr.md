# Notice d'utilisation des outils de l'environnement de développement Riot os

Dans ce tutoriel, nous allons vous présenter l'environnement de développement RiotOS ainsi que l'utilisation de ses outils.

## Sommaire

1. [Makefile](#makefiles)
2. [Compile file](#compilefile)
3. [Execute file](#executefile)


## Arborescence 

Tout d'abord, la structure de votre projet devra respecter une certaine arborescence :

+ Le répertoire contenant votre code, devra être nommé de la même façon que votre application 
	*	Exemple : Si votre application s'appelle ___hello-world___, votre répertoire contenant votre code devra se nommer **hello-world**.
+ Votre code devra comporter un fichier **main.c**.
+ Vous devrez inclure un fichier **Makefile** que l'on détaillera dans la partie [Makefile](#makefiles).

## Makefile <a name = makefiles></a>

L'outil **Makefile** va vous permettre de simplifier la compilation de vos programme en utilisant les fonctionnalités de RiotOS. Pour cela, votre fichier ___Makefile___ devra inclure, au minimum, les paramètres suivants :

+    **APPLICATION** = \<votre\_Nom\_Application> - Ce paramètre sert à définir le nom de votre application. **Le nom doit être le même que celui du répertoire courant**.
+    **BOARD** ?= native - Par défaut, ce paramètre est initialisé à native. Vous pouvez changer ce paramètre pour qu'il corresponde à votre micro-contrôlleur. Il doit apparaître dans la liste des Boards pris en charge par RiotOS(liste disponible à l'adresse ~/RIOT/boards/).
+	**RIOTBASE** ?= $(CURDIR)/../.. - Ce paramètre représente le chemin absolu vers les outils utilisables par RiotOs.Vous serez surement ammenés a changer ce parametre en fonction de la localisation du clone du github de RiotOS.
+	**QUIET** ?= 1 - Si ce paramètre est à 1, alors les opérations réalisé par le Makefile, sont affichées. Si ce paramètre est à 0, alors les opérations sont cachées.
+	**Include $(RIOTBASE)/Makefile.include** - Cette ligne permet d'indiquer le chemin au fichier Makefile.include de RiotOS. Ce fichier permet de comprendre tous les paramètres disponibles avec un fichier Makefile et donc de l'exécuter de la bonne manière.


 
## Compiler un fichier avec _make_ <a name = compilefile></a>

Une fois votre fichier Makefile est configuré avec les paramètres qui vous conviennent, il suffit d'ouvrir un terminal au niveau de celui-ci et d'effectuer la commande :

	make
	
Votre fichier sera alors compilé.

### Le parametre _BOARD_

Il est possible de renseigner un paramètre ___BOARD___ lors de la compilation de votre code avec la commande **make**. Cela permet de compiler votre code pour qu'il puisse s'exécuter sur un micro-contrôleur en particulier. Votre commande ressemblera alors à ceci:

	make BOARD=<votreBoard>
		
Par exemple, votre commande pour compiler avec le board lora-e5-dev sera:

	make BOARD=lora-e5-dev
	
Vous retrouverez votre code compilé dans le répertoire ./bin/<_votreBoard_> (dans notre exemple, ce sera dans le répertoire **./bin/lora-e5-dev/**).



## Exécuter votre fichier compilé <a name = executefile></a>

Maintenant, pour exécuter votre code compilé, vous deverez aller dans le répertoire ./bin/<_board_> contenant votre code compilé grace à la commande:


	cd ./bin/<board>


avec le paramètre **board** que vous avez choisi lors de la compilation avec **make**. Si vous n'avez pas renseigné de paramètre **BOARD** lors de l'utilisation de **make**, le nom de l'emplacement par défaut sera le parmetre BOARD de votre fichier Makefile. Puis, pour exécuter votre code, entrez la commande :


	./<votre_Nom_Application>.elf
