# Instructions for using the tools of the RiotOS development environment

In this tutorial, we will introduce you to RiotOS' development environment and the use of it's tools.

## Summary

1. [Makefile](#makefiles)
2. [Compile file](#compilefile)
3. [Execute file](#executefile)


## Project structure 

First, your project will have to respect a certain structure:

+ The directory containing your code must be named the same as your application 
	*	Example: If your application is called ___hello-world___, the directory containing your code should be called **hello-world**.
+ Your code should include a **main.c** file.
+ You will need to include a **Makefile** file that will be detailed in the [Makefile](#makefiles) section.

## Makefile <a name = makefiles></a>

The **Makefile** tool will allow you to simplify the compilation of your programs using the features of RiotOS. To do this, your ___Makefile___ file must include, minimum following parameters:


+    **APPLICATION = \<your\_Application_Name>** - This parameter is used to define the name of your application. **The name must be the same as the current directory**.
+    **BOARD ?= native** - By default, this parameter is initialized to native. You can change this setting to match with your microcontroller. It should appear in the list of Boards supported by RiotOS (list available at ~/RIOT/boards/).
+	**RIOTBASE ?= $(CURDIR)/../..** - This parameter represents the absolute path to the tools that RiotOs can use.
+	**QUIET ?= 1** - If this parameter is 1, the operations performed by the Makefile are displayed. If this parameter is 0, the operations are hidden.
+	**Include $(RIOTBASE)/Makefile.include** - This line allows you to indicate the path to the RiotOS Makefile.include file. This file makes it possible to understand all the parameters available with a Makefile and therefore to execute it in the right way.


 
## Compile a file with _make_ <a name = compilefile></a>

<<<<<<< HEAD
Once your Makefile has been configured with the parameters that suit your needs, all you have to do is open a terminal at the level of your Makefile and execute the command:
<pre><code># make </code></pre>
Your file will then be compiled.

### The parameter BOARD

It is possible to enter a ___BOARD___ parameter when compiling your code with the **make** command. This compiles your code so that it can run on a particular microcontroler. Your command will then look like this:
<pre><code># make BOARD=\<yourBoard></code></pre>
For example, your command to compile with the lora-e5-dev board would be:
<pre><code># make BOARD=\<lora-e5-dev></code></pre>
You will find your compiled code in the ./bin/<_yourBoard> directory (in our example, it will be in the **./bin/lora-e5-dev/** directory).



## Execute your compiled file <a name = executefile>

Now, to run your compiled code, you will need to go to the ./bin/<_board_> directory containing your compiled code using the command:

	cd ./bin/board
	
Once your Makefile has been configured with the parameters that suit you, all you have to do is open a terminal at the level of this one and execute the command:
		
	make

Your file will then be compiled.

### The parameter _BOARD_

It is possible to enter a ___BOARD___ parameter when compiling your code with the **make** command. This compiles your code so that it can run on a particular micro-contrôller. Your command will then look like this:
		
	make BOARD=<yourBoard>
	

For example, your command to compile with the lora-e5-dev board would be:
		
	make BOARD=<lora-e5-dev>
	
You will find your compiled code in the ./bin/<_yourBoard_> directory (in our example, it will be in the **./bin/lora-e5-dev/** directory).



## Execute your compiled file <a name = executefile></a>

Now, to run your compiled code, you will need to go to the ./bin/<_board_> directory containing your compiled code using the command:

	cd ./bin/<board>/

With the **board** parameter you chose when compiling with **make**. If you did not specify a **BOARD** parameter when using **make**, the default location name will be the BOARD parameter of your Makefile. Then, to run your code, do:

	./<your_Application_Name>.elf
