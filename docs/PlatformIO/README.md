# Installation and Use of PlatformIO

## Table of Contents

1. [Introduction](#Intro)
2. [Installation](#Install)
3. [Usage](#Usage)

## Introduction <a name=Intro></a>

In this folder, you will learn how to install and use **PlatformIO** on **Windows** and **Linux**.

### What is **PlatformIO**?

**PlatformIO** is an open-source, multi-platform integrated development environment (IDE) for programming microcontrollers. It provides a set of tools for developing applications for different microcontrollers, including the ESP32 which we use for the project.

With PlatformIO, you can easily configure compilation and debugging settings, install and manage third-party libraries, and use a variety of debugging tools to solve code problems.

## Installation of **PlatformIO** <a name=Install></a>

You can find installation guides for **PlatformIO** in [English](./INSTALL.md) and [French](./INSTALL.fr.md). Each version contains a section for installation on *Windows* and on *Linux*.

## Usage of **PlatformIO**

You can find usage guides for **PlatformIO** in [English](./TUTORIAL.md) and [French](./TUTORIAL.fr.md).