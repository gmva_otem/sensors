# PlatformIO Usage Guide

## Table of Contents

1. [Introduction](#Intro)
2. [Using PlatformIO in Command Line](#command-line)
3. [Using PlatformIO as an Extension in Visual Studio Code](#vscode)

## Introduction <a name="Intro"></a>

In this guide, we'll introduce you to how to use PlatformIO for developing and deploying an embedded electronics project, such as a temperature sensing project with an ESP32. We will cover two main methods for using PlatformIO: command line (if you want to use PlatformIO from a Docker container) and as an extension for Visual Studio Code.

## Using PlatformIO in Command Line <a name="command-line"></a>

PlatformIO is available as a command-line tool, called `pio`. Here are some common commands to work with PlatformIO in the command line:

1. Initialize a new project:

```
pio project init --board <board_name>
```


Replace `<board_name>` with the name of the board you're using for your project. For example, for an ESP32 board, use `--board esp32dev`.

2. Build the project:

```
pio run
```


This command compiles your project for the board specified in the `platformio.ini` file.

3. Upload the project to the board:

```
pio run --target upload
```


This command compiles your project and uploads it to the board connected to your computer.

4. Clean temporary files and compiled files:

```
pio run --target clean
```

This command removes temporary files and compiled files generated during the build of your project.

## Using PlatformIO as an Extension in Visual Studio Code <a name="vscode"></a>

1. To create a new project, click the "New Project" button in the PlatformIO tab. Select your board and desired framework, then choose a location for your project.

Here is the project in VSCode:

![](./images/vscodeProjet.png)

2. You can now use the buttons located at the bottom of the Visual Studio Code window to build, upload, and clean your project.

Here is the bar with the different tools:

![](./images/vscodeBarre.png)

Here is an example of running the project:

![](./images/vscodeExec.png)

3. To access PlatformIO configuration options, click the gear icon in the PlatformIO sidebar. Here, you can modify the settings for the extension.

Happy developing!
