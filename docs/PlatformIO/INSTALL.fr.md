# Installation de PlatformIO

## Table des matières :
1. [Introduction](#Intro)
2. [Installation de *VSCode* Windows](#InstallVSWindows)
3. [Installation de *VSCode* Linux](#InstallVSLinux)
4. [Ajout de l'extention **PlatformIO** à *VSCode*](#AddExtention)

## Introduction : <a name=Intro></a>

Dans ce document, vous verrez comment installer **PlatformIO** comme extention de **VSCode**. En suivant ce tutoriel d'installation vous verrez donc d'abord l'installation de VSCode sous [Windows](#InstallVSWindows) et sous [Linux](#InstallVSLinux), puis vous verrez comment [ajouter l'extention](#AddExtention) de **PlateformIO** afin de pouvoir l'utiliser.


## Installation pour de VSCode pour Windows :<a name=InstallVSWindows></a>

Pour installer **VSCode** sous Windows, c'est assez simple. Commencez par aller sur le site ofiiciel de VSCode dans la section *download* [ici](https://code.visualstudio.com/download).

Maintenant vous pouvez cliquer sur **Download for Windows**

![](./images/install_vs_windows.png)


Une fois le fichier exécutable télécharger, exécuter le et suivez les étapes d'installations.

![](./images/etape_install_windows.png)

Maintenant vous pouvez vous rendre à [l'ajout de l'extention **PlatformIO** à VSCode](#AddExtention), afin de pouvoir installer et utiliser **PlatformIO**.


## Installation de VSCode pour Linux :<a name=InstallVSLinux></a>

Pour installer **VSCode** sous linux (*Ubuntu*), commencez par ouvrir un terminal de commande. 

1. Pour l'installation, vous aurez besoin de la clé GPG de Microsoft. Pour la récupérer et l'ajouter à votre système, faites les commandes suivantes :

```
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo mv packages.microsoft.gpg /etc/apt/trusted.gpg.d/
```

2. Ajoutez le référentiel Visual Studio Code à la liste de sources de logiciels en utilisant la commande suivante :

```
echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" | sudo tee /etc/apt/sources.list.d/vscode.list
```

3. Mettez à jour la liste des paquets disponibles à partir des référentiels ajoutés en exécutant la commande suivante :

```
sudo apt update
```

Maintenant, vous pouvez installez **VSCode** avec la commande suivante :  

```
sudo apt install code
```

![](./images/install_vs_linux.png)

Une fois installé, vous pouvez le lancer l'IDE avec la commande : *code*.

Maintenant vous pouvez vous rendre à [l'ajout de l'extention **PlatformIO** à VSCode](#AddExtention), afin de pouvoir installer et utiliser **PlatformIO**.




## Ajout de l'extention **PlatformIO** à VSCode :<a name=AddExtention></a>

Pour ajouter l'extention PlatformIO à VSCode, il suffit de cliquer sur le bouttons *Extention* de VSCode, tapez **PlatformIO** dans la barre de rechecherche et finissez en cliquant sur **Installer**. 

![](./images/add_extention.png)

Vous n'avez plus qu'à relancer **VSCode**.

J'espère que ce guide d'installation vous a été utile. Maintenant vous pouvez suivre le [Tutoriel de **PlatformIO**](./TUTORIAL.fr.md).