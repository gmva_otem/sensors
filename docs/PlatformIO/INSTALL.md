# Installation of PlatformIO

## Table of Contents :
1. [Introduction](#Intro)
2. [Installation of *VSCode* for Windows](#InstallVSWindows)
3. [Installation of *VSCode* for Linux](#InstallVSLinux)
4. [Adding the **PlatformIO** extension to *VSCode*](#AddExtension)

## Introduction : <a name=Intro></a>

In this document, you will see how install **PlatformIO** as an extension of VSCode. By following this installation tutorial, you will first see the installation of VSCode on [Windows](#InstallVSWindows) and on [Linux](#InstallVSLinux), then you will see how to [add the extension](#AddExtension) of **PlatformIO** so that you can use it.

## Installation of VSCode for Windows :<a name=InstallVSWindows></a>

To install **VSCode** on Windows, it's quite simple. Start by going to the official VSCode website in the *download* section [here](https://code.visualstudio.com/download).

Now you can click on **Download for Windows**

![](./images/install_vs_windows.png)

Once the executable file is downloaded, run it and follow the installation steps.

![](./images/etape_install_windows.png)

Now you can go to [adding the **PlatformIO** extension to VSCode](#AddExtension) in order to be able to install and use **PlatformIO**.

## Installation of VSCode for Linux :<a name=InstallVSLinux></a>

To install **VSCode** on Linux (*Ubuntu*), start by opening a command terminal.

1. For the installation, you will need the Microsoft GPG key. To retrieve and add it to your system, do the following commands:

```
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo mv packages.microsoft.gpg /etc/apt/trusted.gpg.d/
```


2. Add the Visual Studio Code repository to the list of software sources using the following command:

```
echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" | sudo tee /etc/apt/sources.list.d/vscode.list
```


3. Update the list of available packages from the added repositories by running the following command:

```
sudo apt update
```


Now, you can install **VSCode** with the following command:

```
sudo apt install code
```


![](./images/install_vs_linux.png)

Once installed, you can launch the IDE with the command: *code*.

Now you can go to [adding the **PlatformIO** extension to VSCode](#AddExtension) in order to be able to install and use **PlatformIO**.

## Adding the **PlatformIO** extension to VSCode :<a name=AddExtension></a>

To add the PlatformIO extension to VSCode, simply click on the *Extension* button in VSCode, type **PlatformIO** in the search bar, and finish by clicking **Install**.

![](./images/add_extention.png)

You just have to restart **VSCode**.

I hope this installation guide was helpful. Now You can follow the [Tutorial of **PlatformIO**](./TUTORIAL.md).

