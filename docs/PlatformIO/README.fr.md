# Installation et utilisation de PlatformIO

## Table des matières

1. [Introduction](#Intro)
2. [Installation](#Install)
3. [Utilisation](#Utilisation)

## Introduction <a name=Intro></a>

Dans ce dossier, vous allez apprendre à installer et utiliser **PlatformIO** sous **Windows** et **Linux**.

### Qu'est ce que **PlatformIO** ?

**PlatformIO** est un environnement de développement intégré (IDE) open-source multiplateforme pour la programmation de microcontrôleurs. Il fournit un ensemble d'outils pour développer des applications pour différents microcontrôleurs, notamment l'ESP32 que nous utilisons pour le projet.

Grâce à PlatformIO, vous pourrez facilement configurer les paramètres de compilation et de débogage, installer et gérer des bibliothèques tierces, et utiliser une variété d'outils de débogage pour résoudre les problèmes de code.

## Installation de **PlatformIO** <a name=Install></a>

Vous pouvez retrouver des guides d'installations de **PlatformIO** en [anglais](./INSTALL.md) et en [français](./INSTALL.fr.md). Dans chacune de ses versions, il y a une partie pour l'installation sous *Windows* et sous *Linux*.

## Utilisation de **PlatformIO**

Vous pouvez retrouver des guides d'utilisation de **PlatformIO** en [anglais](./TUTORIAL.md) et en [français](./TUTORIAL.fr.md).
