# Guide d'utilisation de PlatformIO

## Table des matières

1. [Introduction](#Intro)
2. [Utilisation de PlatformIO en ligne de commande](#ligne-de-commande)
3. [Utilisation de PlatformIO en tant qu'extension sur Visual Studio Code](#vscode)

## Introduction <a name="Intro"></a>

Dans ce guide, nous vous présenterons comment utiliser PlatformIO pour développer et déployer un projet d'électronique embarquée, tel que le projet de collecte de température avec un ESP32. Nous aborderons deux méthodes principales pour utiliser PlatformIO : en ligne de commande (si vous voulez utilisé PlatformIO depuis un conteneur docker) et en tant qu'extension pour Visual Studio Code.

## Utilisation de PlatformIO en ligne de commande <a name="ligne-de-commande"></a>

PlatformIO est disponible en tant qu'outil en ligne de commande, appelé `pio`. Voici quelques commandes courantes pour travailler avec PlatformIO en ligne de commande :

1. Initialiser un nouveau projet :

```
pio project init --board <nom_du_board>
```

Remplacez `<nom_du_board>` par le nom de la carte que vous utilisez pour votre projet. Par exemple, pour une carte ESP32, utilisez `--board esp32dev`.


2. Compiler le projet :

```
pio run
```

Cette commande compile votre projet pour la carte spécifiée dans le fichier `platformio.ini`.


3. Téléverser le projet sur la carte :

```
pio run --target upload
```

Cette commande compile votre projet et le téléverse sur la carte connectée à votre ordinateur.

4. Nettoyer les fichiers temporaires et les fichiers compilés :

```
pio run --target clean
```

Cette commande supprime les fichiers temporaires et les fichiers compilés générés lors de la construction de votre projet.



## Utilisation de PlatformIO en tant qu'extension sur Visual Studio Code <a name="vscode"></a>



1. Pour créer un nouveau projet, cliquez sur le bouton "New Project" (nouveau projet) dans l'onglet PlatformIO. Sélectionnez votre carte et le framework souhaité, puis choisissez un emplacement pour votre projet.

Voici le projet dans VSCode :

![](./images/vscodeProjet.png)

2. Vous pouvez maintenant utiliser les boutons situés en bas de la fenêtre de Visual Studio Code pour compiler, téléverser et nettoyer votre projet.

Voici la barre avec les différents outils :

![](./images/vscodeBarre.png)

Voici un exemple d'exécution  du projet :

![](./images/vscodeExec.png)

3. Pour accéder aux options de configuration de PlatformIO, cliquez sur l'icône en forme d'engrenage dans la barre latérale PlatformIO. Ici, vous pouvez modifier les paramètres de l'extension.



Bon développement