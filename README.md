
Le projet à pour objectif de suivre et de valoriser des données de
suivi des températures d'eau de mer du Golfe du Morbihan pour des recherches sur l'impact du réchauffement climatique dans le Golfe du Morbihan.
Ce suivi pourra ainsi être intégré à l'Observatoire des effets du changement
climatique porté par Golfe du Morbihan Vannes Agglomération (GMVA).

La branche Capteur de ce projet rassemble deux équipes de quatre, le fichier ACTORS.md
peut être consulté pour voir les membres de chaque groupe de la branche.

Dans ce projet, il s'agit de développer la solution qui permettera
de relever les températures d'eau de mer du Golfe du Morbihan à
différents niveaux de profondeur.

Pour ce faire, il faudra

* choisir le matériel approprié à la collecte d'information
  (capteurs, carte micro-contrôleur, antennes, alimentation, etc.).
* développer une idée de prototype et câbler le matériel ensemble.
* développer le logiciel à installer sur ces cartes à base de
  micro-contrôleur pour acquérir les données depuis les capteurs et de
  remonter ces données vers une passerelles de collecte.
* développer de la documentations sur l'installation et l'utilisation du logiciel
et créer un docker pour une installation simplifiée.
* rédiger un programme sur le logiciel et le mettre sur le micro-contrôleur pour
la récolte de données.
* rédiger un programme sur le logiciel et le mettre sur le micro-contrôleur pour
la gestion de la mise en veille.
* tester en conditions réelles ce prototype.

Le développement se fera en C sur le logiciel STM32CubeIDE. Le protocole de communication utilisé
pour transmettre les données sera le protocole LoRaWAN, qui repose sur
la technologie radio faible débit et longue portée LoRa.

Les capteurs devant rester plusieurs années dans l'environnement, il est nécessaire
de choisir du matériel économe en énergie. Il faudra en outre gérer
convenablement la mise en sommeil du matériel, et définir les
fréquences de collecte et de remontée des données.

* La fréquence de collecte de données a été définie à 1 collecte toutes les heures.
* La fréquence d'envoi de données a été définie à 1 envoi toutes les 6 heures.

Un test de prototype est prévu le Vendredi 20 Janvier 2023.
