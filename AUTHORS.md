Liste des membres du projet OTEM - partie Capteurs :

* **Jules Gauthier** - Responsable Virtualisation et Documentation

* **Samuel Tabor** - Responsable Virtualisation et Test

* **Nolan Jametti** - Responsable Développement

* **Nolann Nougues** - Responsable Communication 

Liste des membres du projet OTEM - partie Antenne :

* **Thomas Largillet** (Fozl/Mosthal) - Responsable Développement

* **Hamza Marghich** - Responsable Matériel et Documentation

* **Léo Joly--Jehenne** - Scrum Master / Responsable Communication

* **Hugo Perrel--Le Jallé** - Responsable des Tests et Gitlab
